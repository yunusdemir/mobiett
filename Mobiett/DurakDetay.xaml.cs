﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.Serialization.Json;
using System.Windows.Media;
using System.IO;
using System.Text;
using System.Windows.Media.Imaging;
using System.IO.IsolatedStorage;
using System.Device.Location;
using System.Diagnostics;
using System.Windows.Input;
using Newtonsoft.Json;
using Microsoft.Phone.Controls;
using Mobiett.Framework;
using Mobiett.Models;
using Mobiett.Resources;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace Mobiett
{
    public partial class DurakDetay : PhoneApplicationPage
    {
        private string url = "http://159.8.34.170:8181";
        private int pointI = 0;
        private int pointLength = 1;
        string msg = "";
        private string id = "";
        private string code = "";
        private List<BuslineByStopModel.BuslineByStop> busLins;
        private List<BusStopsLineModel.BusStopsLine> mybusLins;
        private List<BusStopsLineModel.BusStopsLine> myList = new List<BusStopsLineModel.BusStopsLine>();
        private List<string> mybuttons;
        public string btnBackground;
        public string btnBackgroundSelected;
        private FrameworkElement _feContainer;
        private BusStop mybus = new BusStop();
        private Uri uri = new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute);

        public Thickness myMarginOne;



        public DurakDetay()
        {
            InitializeComponent();
            _feContainer = this.ButtonsPane as FrameworkElement;
            mybusLins = new List<BusStopsLineModel.BusStopsLine>();
            busLins = new List<BuslineByStopModel.BuslineByStop>();
            mybuttons = new List<string>();
            btnBackground = "/Mobiett;component/Images/segment@2x.png";
            btnBackgroundSelected = "/Mobiett;component/Images/segment_selected2x.png";
            myMarginOne = new Thickness(5, -30, 0, 0);
        }

        private void listBox2_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //BusStopsLineModel.BusStopsLine b = listBox1.SelectedItem as BusStopsLineModel.BusStopsLine;
            //string _msg = b.line.code + "," + b.line.name + " , " + b.line.directions + " , " + b.line.id;
            //if (b.dataType == "BATCH")
            //{
            //    NavigationService.Navigate(new Uri("/OtobusDetay.xaml?msg=" + _msg + "&dataType=" + "Batch", UriKind.Relative));
            //}
            //else
            //{
            //    NavigationService.Navigate(new Uri("/OtobusDetay.xaml?msg=" + _msg + "&dataType=" + "Live", UriKind.Relative));
            //}
        }

        private void image3_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri uri = new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }


        public class myJsonData { public string methodType; public string url; public Dictionary<string, string> headerMap;}

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {

            if (!String.IsNullOrEmpty(this.code))
            {
                getStopClass(this.code);
            }
            else
            {
                if (NavigationContext.QueryString.TryGetValue("msg", out msg))
                {
                    var _msgArray = msg.Split(',');
                    textBlock1.Text = _msgArray[0] + LangugeResource.DurakDetay_Stops;
                    textBlockId.Text = _msgArray[1] + "";
                    textBlock3.Text = _msgArray[2] + "";
                    this.code = _msgArray[3];
                    this.id = _msgArray[1].Trim();
                    getStopClass(code);
                }
            }
            mybusStops();
            var _id = textBlockId.Text.Trim();
            //var _id = "624";
            DownpageLoaded(code);


            HttpOperation.SendRequest<object, List<BuslineByStopModel.BuslineByStop>>(null, "/busstops/code/" + code.ToLower() + "/buslines?cityId=" + City.Current.id, "GET", Response_Completed1);
            animStart();
        }

        private void mybusStops()
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains("mybusStops"))
            {
                var busList = (Dictionary<string, List<BusStop>>)IsolatedStorageSettings.ApplicationSettings["mybusStops"];
                if (busList.ContainsKey(City.Current.id) && (busList[City.Current.id] == null || busList[City.Current.id].Count == 0))
                {
                    busList.Remove(City.Current.id);
                    Dispatcher.BeginInvoke(() => image2.Visibility = Visibility.Visible);
                    Dispatcher.BeginInvoke(() => imageStar.Visibility = Visibility.Collapsed);
                    return;
                }
                if (busList[City.Current.id].Any(x => x.id == this.id))
                {
                    Dispatcher.BeginInvoke(() => imageStar.Visibility = Visibility.Visible);
                    Dispatcher.BeginInvoke(() => image2.Visibility = Visibility.Collapsed);
                }
                else
                {
                    Dispatcher.BeginInvoke(() => image2.Visibility = Visibility.Visible);
                    Dispatcher.BeginInvoke(() => imageStar.Visibility = Visibility.Collapsed);
                }
            }
            else
            {
                Dispatcher.BeginInvoke(() => image2.Visibility = Visibility.Visible);
                Dispatcher.BeginInvoke(() => imageStar.Visibility = Visibility.Collapsed);
            }
        }

        private void DownpageLoaded(string _code)
        {
            HttpOperation.SendRequest<object>(null, string.Format("/busstops/code/{0}/buses?cityId={1}", _code.ToLower(), City.Current.id), "GET", Response_Completed2);
            animStart();
        }

        private void Response_Completed1(List<BuslineByStopModel.BuslineByStop> busLines, Exception exception)
        {
            if (exception != null)
            {
                MessageBox.Show(exception.Message);
                return;
            }
            AnimStop();
            this.busLins = busLines;
            var busLength = busLines.Count();
            int busMode = busLength / 12;

            //pointLength kullanılacak
            if (busLength > 12)
            {
                this.pointLength = busMode * 12 < busLength ? busMode + 1 : busMode;
            }
            var _margin = (480 - (this.pointLength * 55)) / 2;
            Dispatcher.BeginInvoke(() => listBox3.Margin = new System.Windows.Thickness(_margin, 0, 0, 0));

            SetButtonsContent(busLines, this.pointI * 12);
            // Need to handle the exception some other way 
        }
        private void ClearButtonsContent()
        {
            Dispatcher.BeginInvoke(() => button1.Content = "");
            Dispatcher.BeginInvoke(() => button2.Content = "");
            Dispatcher.BeginInvoke(() => button3.Content = "");
            Dispatcher.BeginInvoke(() => button4.Content = "");
            Dispatcher.BeginInvoke(() => button5.Content = "");
            Dispatcher.BeginInvoke(() => button6.Content = "");
            Dispatcher.BeginInvoke(() => button7.Content = "");
            Dispatcher.BeginInvoke(() => button8.Content = "");
            Dispatcher.BeginInvoke(() => button9.Content = "");
            Dispatcher.BeginInvoke(() => button10.Content = "");
            Dispatcher.BeginInvoke(() => button11.Content = "");
            Dispatcher.BeginInvoke(() => button12.Content = "");


        }

        public void ClearButtonsBackground()
        {
            Dispatcher.BeginInvoke(() =>
            {
                ImageBrush brushe = new ImageBrush();
                brushe.ImageSource = new BitmapImage(new Uri(btnBackground, UriKind.Relative));
                button1.Background = brushe;
                button2.Background = brushe;
                button3.Background = brushe;
                button4.Background = brushe;
                button5.Background = brushe;
                button6.Background = brushe;
                button7.Background = brushe;
                button8.Background = brushe;
                button9.Background = brushe;
                button10.Background = brushe;
                button11.Background = brushe;
                button12.Background = brushe;
                button1.Foreground = new SolidColorBrush(Colors.Black);
                button2.Foreground = new SolidColorBrush(Colors.Black);
                button3.Foreground = new SolidColorBrush(Colors.Black);
                button4.Foreground = new SolidColorBrush(Colors.Black);
                button5.Foreground = new SolidColorBrush(Colors.Black);
                button6.Foreground = new SolidColorBrush(Colors.Black);
                button7.Foreground = new SolidColorBrush(Colors.Black);
                button8.Foreground = new SolidColorBrush(Colors.Black);
                button9.Foreground = new SolidColorBrush(Colors.Black);
                button10.Foreground = new SolidColorBrush(Colors.Black);
                button11.Foreground = new SolidColorBrush(Colors.Black);
                button12.Foreground = new SolidColorBrush(Colors.Black);
            });
        }

        private void SetButtonsContent(List<BuslineByStopModel.BuslineByStop> busLines, int pointIndex = 0)
        {

            ClearButtonsContent();
            ClearButtonsBackground();
            if (busLines.Count >= pointIndex + 1)
            {
                if (this.mybuttons.Count != 0 && this.mybuttons.Any(x => x.Equals(busLines.ElementAt(0 + pointIndex).code)))
                {
                    Dispatcher.BeginInvoke(() =>
                        {
                            var brush = new ImageBrush();
                            brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                            button1.Background = brush;
                            button1.Foreground = new SolidColorBrush(Colors.White);
                        });
                }
                Dispatcher.BeginInvoke(() => button1.Content = busLines.ElementAt(0 + pointIndex).code);
            }
            if (busLines.Count >= pointIndex + 2)
            {
                if (this.mybuttons.Count != 0 && this.mybuttons.Any(x => x.Equals(busLines.ElementAt(1 + pointIndex).code)))
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        var brush = new ImageBrush();
                        brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                        button2.Background = brush;
                        button2.Foreground = new SolidColorBrush(Colors.White);
                    });
                }
                Dispatcher.BeginInvoke(() => button2.Content = busLines.ElementAt(1 + pointIndex).code);
            }
            if (busLines.Count >= pointIndex + 3)
            {
                if (this.mybuttons.Count != 0 && this.mybuttons.Any(x => x.Equals(busLines.ElementAt(2 + pointIndex).code)))
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        var brush = new ImageBrush();
                        brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                        button3.Background = brush;
                        button3.Foreground = new SolidColorBrush(Colors.White);
                    });
                }
                Dispatcher.BeginInvoke(() => button3.Content = busLines.ElementAt(2 + pointIndex).code);
            }
            if (busLines.Count >= pointIndex + 4)
            {
                if (this.mybuttons.Count != 0 && this.mybuttons.Any(x => x.Equals(busLines.ElementAt(3 + pointIndex).code)))
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        var brush = new ImageBrush();
                        brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                        button4.Background = brush;
                        button4.Foreground = new SolidColorBrush(Colors.White);
                    });
                }
                Dispatcher.BeginInvoke(() => button4.Content = busLines.ElementAt(3 + pointIndex).code);
            }
            if (busLines.Count >= pointIndex + 5)
            {
                if (this.mybuttons.Count != 0 && this.mybuttons.Any(x => x.Equals(busLines.ElementAt(4 + pointIndex).code)))
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        var brush = new ImageBrush();
                        brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                        button5.Background = brush;
                        button5.Foreground = new SolidColorBrush(Colors.White);
                    });
                }
                Dispatcher.BeginInvoke(() => button5.Content = busLines.ElementAt(4 + pointIndex).code);
            }
            if (busLines.Count >= pointIndex + 6)
            {
                if (this.mybuttons.Count != 0 && this.mybuttons.Any(x => x.Equals(busLines.ElementAt(5 + pointIndex).code)))
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        var brush = new ImageBrush();
                        brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                        button6.Background = brush;
                        button6.Foreground = new SolidColorBrush(Colors.White);
                    });
                }
                Dispatcher.BeginInvoke(() => button6.Content = busLines.ElementAt(5 + pointIndex).code);
            }
            if (busLines.Count >= pointIndex + 7)
            {
                if (this.mybuttons.Count != 0 && this.mybuttons.Any(x => x.Equals(busLines.ElementAt(6 + pointIndex).code)))
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        var brush = new ImageBrush();
                        brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                        button7.Background = brush;
                        button7.Foreground = new SolidColorBrush(Colors.White);
                    });
                }
                Dispatcher.BeginInvoke(() => button7.Content = busLines.ElementAt(6 + pointIndex).code);
            }
            if (busLines.Count >= pointIndex + 8)
            {
                if (this.mybuttons.Count != 0 && this.mybuttons.Any(x => x.Equals(busLines.ElementAt(7 + pointIndex).code)))
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        var brush = new ImageBrush();
                        brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                        button8.Background = brush;
                        button8.Foreground = new SolidColorBrush(Colors.White);
                    });
                }
                Dispatcher.BeginInvoke(() => button8.Content = busLines.ElementAt(7 + pointIndex).code);
            }
            if (busLines.Count >= pointIndex + 9)
            {
                if (this.mybuttons.Count != 0 && this.mybuttons.Any(x => x.Equals(busLines.ElementAt(8 + pointIndex).code)))
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        var brush = new ImageBrush();
                        brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                        button9.Background = brush;
                        button9.Foreground = new SolidColorBrush(Colors.White);
                    });
                }
                Dispatcher.BeginInvoke(() => button9.Content = busLines.ElementAt(8 + pointIndex).code);
            }
            if (busLines.Count >= pointIndex + 10)
            {
                if (this.mybuttons.Count != 0 && this.mybuttons.Any(x => x.Equals(busLines.ElementAt(9 + pointIndex).code)))
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        var brush = new ImageBrush();
                        brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                        button10.Background = brush;
                        button10.Foreground = new SolidColorBrush(Colors.White);
                    });
                }
                Dispatcher.BeginInvoke(() => button10.Content = busLines.ElementAt(9 + pointIndex).code);
            }
            if (busLines.Count >= pointIndex + 11)
            {
                if (this.mybuttons.Count != 0 && this.mybuttons.Any(x => x.Equals(busLines.ElementAt(10 + pointIndex).code)))
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        var brush = new ImageBrush();
                        brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                        button11.Background = brush;
                        button11.Foreground = new SolidColorBrush(Colors.White);
                    });
                }
                Dispatcher.BeginInvoke(() => button11.Content = busLines.ElementAt(10 + pointIndex).code);
            }
            if (busLines.Count >= pointIndex + 12)
            {
                if (this.mybuttons.Count != 0 && this.mybuttons.Any(x => x.Equals(busLines.ElementAt(11 + pointIndex).code)))
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        var brush = new ImageBrush();
                        brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                        button12.Background = brush;
                        button12.Foreground = new SolidColorBrush(Colors.White);
                    });
                }
                Dispatcher.BeginInvoke(() => button12.Content = busLines.ElementAt(11 + pointIndex).code);
            }
            List<PointsClass> myPoints = new List<PointsClass>();
            var point = new PointsClass();
            for (int i = 0;i < this.pointLength;i++)
            {
                var _source = "/Mobiett;component/Images/pager_empty@2x.png";
                if (this.pointI == i)
                {
                    _source = "/Mobiett;component/Images/pager_full@2x.png";
                }

                switch (i)
                {
                    case 0:
                        point.imgSource = _source;
                        point.column = i.ToString();
                        break;
                    case 1:
                        point.imgSource1 = _source;
                        point.column1 = i.ToString();
                        break;
                    case 2:
                        point.imgSource2 = _source;
                        point.column2 = i.ToString();
                        break;
                    case 3:
                        point.imgSource3 = _source;
                        point.column3 = i.ToString();
                        break;
                    case 4:
                        point.imgSource4 = _source;
                        point.column4 = i.ToString();
                        break;
                    case 5:
                        point.imgSource5 = _source;
                        point.column5 = i.ToString();
                        break;
                    case 6:
                        point.imgSource6 = _source;
                        point.column6 = i.ToString();
                        break;
                    default:
                        break;
                }
            }
            myPoints.Add(point);
            Dispatcher.BeginInvoke(() => listBox3.ItemsSource = myPoints);
        }

        private void Response_Completed2(string duraklarvar, Exception exception)
        {
            AnimStop();

            byte[] duraklarByte = Encoding.UTF8.GetBytes(duraklarvar);
            MemoryStream duraklarStr = new MemoryStream(duraklarByte);
            duraklarStr.Position = 0;
            byte[] encryptedData = new byte[duraklarStr.Length];
            duraklarStr.Read(encryptedData, 0, (int)duraklarStr.Length);

            // Convert the decrypted data to a string value...
            var result = Convert.ToBase64String(encryptedData);
            var finalReslt = Convert.FromBase64String(result);
            var str = Encoding.UTF8.GetString(finalReslt, 0, finalReslt.Length);
            //string _jsonStr=streamReader.(duraklarStr
            DataContractJsonSerializer duraklarSerialzer = new DataContractJsonSerializer(typeof(List<BusStopsLineModel.BusStopsLine>));
            List<BusStopsLineModel.BusStopsLine> busstopsLine = (List<BusStopsLineModel.BusStopsLine>)duraklarSerialzer.ReadObject(duraklarStr);
            int j = 0;
            Dispatcher.BeginInvoke(() => listBox1.ItemsSource = busstopsLine);
            Dispatcher.BeginInvoke(() => image2.Visibility = Visibility.Visible);
            foreach (var item in busstopsLine)
            {
                string _name = item.line.name;
                if (_name.Length > 26)
                {
                    item.line.name = item.line.name.Substring(0, 22) + "...";
                }
                if (item.dataType == "BATCH")
                {
                    item.line.typeMode = LangugeResource.DurakDetay_LineCalender;
                    item.line.kalanSure = "";
                }
                else
                {
                    item.line.kalanSure = "" + item.timeToStop + " dk";
                    item.line.typeMode = "" + item.line.name;
                }
                if (j % 2 == 0)
                {
                    item.color = "#FFF7F7F7";
                }
                else
                {
                    item.color = "#FFF";
                }
                if (item.properties.Count != 0)
                {
                    for (int a = 0;a < item.properties.Count;a++)
                    {
                        switch (a)
                        {
                            case 0:
                                item.line.prop0 = "/Mobiett;component/Images/" + item.properties.ElementAt(a).Split('/').Last();
                                break;
                            case 1:
                                item.line.prop1 = "/Mobiett;component/Images/" + item.properties.ElementAt(a).Split('/').Last();
                                break;
                            case 2:
                                item.line.prop2 = "/Mobiett;component/Images/" + item.properties.ElementAt(a).Split('/').Last();
                                break;
                            case 3:
                                item.line.prop3 = "/Mobiett;component/Images/" + item.properties.ElementAt(a).Split('/').Last();
                                break;
                        }
                    }
                    this.myMarginOne = new Thickness(5, -30, 0, 0);
                }
                else
                {
                    this.myMarginOne = new Thickness(5, -30, 0, 0);
                }
                Dispatcher.BeginInvoke(() => listBox1.ItemsSource = busstopsLine);
                j++;
            }
            this.mybusLins = busstopsLine;
            Dispatcher.BeginInvoke(() => listBox1.ItemsSource = busstopsLine);

        }

        private void GestureListener_Flick(object sender, FlickGestureEventArgs e)
        {
            if (e.Direction == System.Windows.Controls.Orientation.Horizontal)
            {
                if (e.HorizontalVelocity < 0)
                {
                    if (this.pointI + 1 < this.pointLength)
                    {
                        this.pointI++;
                        SetButtonsContent(this.busLins, this.pointI * 12);
                        //ClearButtonsBackground();
                    }
                    // flick right
                }
                else
                {
                    if (this.pointI != 0)
                    {
                        this.pointI--;
                        SetButtonsContent(this.busLins, this.pointI * 12);
                    }
                    // flick left
                }
            }
        }

        private void GestureListener_OnDragDelta(object sender, DragDeltaGestureEventArgs e)
        {
            double offset = _feContainer.GetHorizontalOffset().Value + e.HorizontalChange;
            _feContainer.SetHorizontalOffset(offset);

        }

        private void GestureListener_OnDragCompleted(object sender, DragCompletedGestureEventArgs e)
        {

        }

        private void listBox3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //BusStops m = listBox1.SelectedItem as BusStops;
            //string _msg = m.busStop.name + "," + m.busStop.id + " , " + m.busStop.directionDescription;
            //NavigationService.Navigate(new Uri("/DurakDetay.xaml?msg=" + _msg, UriKind.Relative));
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            var lastPage = NavigationService.BackStack.FirstOrDefault();
            var _uri = new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute);
            if (lastPage != null && lastPage.Source.Equals(_uri))
            {
                this.uri = lastPage.Source;
                NavigationService.Navigate(this.uri);
            }
            else
            {
            }
        }
        // Sayfalararası bilgi/data aktarmak için

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            //myUserControl myUserControl = new MyUserControl;
            //Style style = MyStackPanel.FindResource("ucStyle") as Style;
            //StackPanel panel = new StackPanel();
            //TextBlock txt = panel.FindName("txtCode") as TextBlock; 
            if (NavigationContext.QueryString.TryGetValue("msg", out msg))
            {
                var _msgArray = msg.Split(',');
                textBlock1.Text = _msgArray[0] + LangugeResource.DurakDetay_Stops;
                textBlockId.Text = _msgArray[1] + "";
                textBlock3.Text = _msgArray[2] + "";
                this.id = _msgArray[1].Trim();
            }
        }
        public class PointsClass
        {
            public string imgSource { get; set; }
            public string imgSource1 { get; set; }
            public string imgSource2 { get; set; }
            public string imgSource3 { get; set; }
            public string imgSource4 { get; set; }
            public string imgSource5 { get; set; }
            public string imgSource6 { get; set; }
            public string column { get; set; }
            public string column1 { get; set; }
            public string column2 { get; set; }
            public string column3 { get; set; }
            public string column4 { get; set; }
            public string column5 { get; set; }
            public string column6 { get; set; }
        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //BusStopsLineModel.BusStopsLine b = listBox1.SelectedItem as BusStopsLineModel.BusStopsLine;
            //if (b != null)
            //{
            //    string _msg = b.line.code + "," + b.line.name + " , " + b.line.directions + " , " + b.line.id;
            //    if (b.dataType == "BATCH")
            //    {
            //        NavigationService.Navigate(new Uri("/SeferSaatleri.xaml?msg=" + _msg + "&dataType=" + "Batch", UriKind.Relative));
            //    }
            //    else
            //    {
            //        NavigationService.Navigate(new Uri("/OtobusDetay.xaml?msg=" + _msg + "&dataType=" + "Live", UriKind.Relative));
            //    }
            //}
        }

        private void image2_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

            if (IsolatedStorageSettings.ApplicationSettings.Contains("mybusStops"))
            {
                var busList = (Dictionary<string, List<Models.BusStop>>)IsolatedStorageSettings.ApplicationSettings["mybusStops"];
                if (!busList.ContainsKey(City.Current.id))
                    busList.Add(City.Current.id, new List<BusStop>());
                busList[City.Current.id].Add(this.mybus);
                IsolatedStorageSettings.ApplicationSettings["mybusStops"] = busList;
            }
            else
            {
                IsolatedStorageSettings.ApplicationSettings["mybusStops"] = new Dictionary<string, List<Models.BusStop>>();
                var busList = (Dictionary<string, List<Models.BusStop>>)IsolatedStorageSettings.ApplicationSettings["mybusStops"];
                busList.Add(City.Current.id, new List<BusStop> { this.mybus });
                IsolatedStorageSettings.ApplicationSettings["mybusStops"] = busList;
            }
            Dispatcher.BeginInvoke(() => imageStar.Visibility = Visibility.Visible);
            Dispatcher.BeginInvoke(() => image2.Visibility = Visibility.Collapsed);
            MessageBox.Show(LangugeResource.PopupMessage_AddToFav);
        }

        private void getStopClass(string _id)
        {
            HttpOperation.SendRequest<object, BusStop>(null, "/busstops/code/" + _id.ToLower() + "?cityId=" + City.Current.id, "GET", Response_Completed4);
            animStart();
        }

        private void Response_Completed4(BusStop busStop, Exception arg2)
        {
            if (arg2 != null)
            {
                return;
            }
            this.mybus = busStop;
        }


        private void imageStar_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var busList = (Dictionary<string, List<BusStop>>)IsolatedStorageSettings.ApplicationSettings["mybusStops"];
            busList[City.Current.id].RemoveAll(q => q.id == mybus.id);
            MessageBox.Show(LangugeResource.PopupMessage_RemoveFromFav);
            Dispatcher.BeginInvoke(() => image2.Visibility = Visibility.Visible);
            Dispatcher.BeginInvoke(() => imageStar.Visibility = Visibility.Collapsed);
        }

        private void button1_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var button = (sender as Button);
            string _content = button.Content.ToString();

            if (this.mybuttons.Count != 0)
            {
                if (this.mybuttons.Any(x => x.Equals(_content)))
                {
                    this.mybuttons.Remove(_content);
                    var brush = new ImageBrush();
                    brush.ImageSource = new BitmapImage(new Uri(btnBackground, UriKind.Relative));
                    (sender as Button).Foreground = new SolidColorBrush(Colors.Black);
                    button.Background = brush;

                }
                else
                {
                    if (!button.Content.Equals(""))
                    {
                        var brush = new ImageBrush();
                        brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                        button.Background = brush;
                        (sender as Button).Foreground = new SolidColorBrush(Colors.White);
                        this.mybuttons.Add(_content);
                    }
                }
            }
            else
            {
                if (!button.Content.Equals(""))
                {
                    var brush = new ImageBrush();
                    brush.ImageSource = new BitmapImage(new Uri(btnBackgroundSelected, UriKind.Relative));
                    button.Background = brush;
                    (sender as Button).Foreground = new SolidColorBrush(Colors.White);
                    this.mybuttons.Add(_content);
                }
            }
            for (int i = 0;i < this.mybuttons.Count;i++)
            {
                if (i == 0)
                {
                    var _list = this.mybusLins.Where(x => x.line.code.Equals(this.mybuttons[i]));
                    myList = _list.ToList();
                }
                else
                {
                    var _list = this.mybusLins.Where(x => x.line.code.Equals(this.mybuttons[i]));
                    myList = myList.Concat(_list).ToList();
                }
            }
            if (this.mybuttons.Count == 0)
            {
                myList = this.mybusLins.ToList();
            }
            int j = 0;
            foreach (var item in myList)
            {
                if (j % 2 == 0)
                {
                    item.color = "#FFF7F7F7";
                }
                else
                {
                    item.color = "#FFF";
                }
                j++;
            }
            Dispatcher.BeginInvoke(() => listBox1.ItemsSource = myList);
        }

        private int ca = 2;
        private void Anime_Completed(object sender, EventArgs e)
        {
            string url = "/Mobiett;component/Images/Loading/loading" + ca + ".png";
            Dispatcher.BeginInvoke(() =>
            {
                imageAnime.Source = new BitmapImage(new Uri(url, UriKind.Relative));
                if (imageAnime.Visibility == Visibility.Visible)
                {
                    ca++;
                    if (ca == 46)
                    {
                        ca = 0;
                    }
                    StoryBoardA.Begin();
                }
            });
        }

        private void animStart()
        {
            Dispatcher.BeginInvoke(() =>
            {
                loadAnim.Visibility = Visibility.Visible;
                StoryBoardA.Begin();
            });
        }

        private void AnimStop()
        {

            Dispatcher.BeginInvoke(() =>
            {
                StoryBoardA.Stop();
                loadAnim.Visibility = Visibility.Collapsed;
            });
        }

        private void ListBox1_OnManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            var j = JsonConvert.SerializeObject(e);
            Debug.WriteLine(j);
        }

        private void refreshPanelRefreshRequested(object sender, EventArgs e)
        {
            var _id = textBlockId.Text.Trim();
            //var _id = "624";
            DownpageLoaded(this.code);
        }

        private void ImageOnTap(object sender, GestureEventArgs e)
        {
            Grid g = sender as Grid;
            BusStopsLineModel.BusStopsLine b = g.Tag as BusStopsLineModel.BusStopsLine;
            if (b != null)
            {
                string _msg = b.line.code + "," + b.line.name + " , " + b.line.directions + " , " + b.line.id;
                if (b.dataType == "BATCH")
                {
                    NavigationService.Navigate(new Uri("/SeferSaatleri.xaml?msg=" + _msg + "&dataType=" + "Batch", UriKind.Relative));
                }
                else
                {
                    NavigationService.Navigate(new Uri("/OtobusDetay.xaml?msg=" + _msg + "&dataType=" + "Live", UriKind.Relative));
                }
            }
        }
    }
}