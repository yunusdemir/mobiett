﻿using System;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;


namespace Mobiett
{
    public partial class DilContent : PhoneApplicationPage
    {
        String[] Diller = { "Türkçe","English",
                              "العربية"};

        public DilContent()
        {
            InitializeComponent();
            this.dilList.ItemsSource = Diller;
        }
     
    }
}