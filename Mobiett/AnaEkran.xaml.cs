﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Runtime.Serialization.Json;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.IO;
using System.Text;
using System.Windows.Media.Imaging;
using System.Device.Location;
using System.Diagnostics;
using System.Globalization;
using Windows.Devices.Geolocation;
using Newtonsoft.Json;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Maps;
using System.IO.IsolatedStorage;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.Resources;
using Mobiett.Framework;
using Mobiett.Models;
using System.Windows.Controls.Primitives;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Mobiett.Resources;
using Newtonsoft.Json.Linq;
using Wp7nl.Utilities;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace Mobiett
{
    public partial class AnaEkran : PhoneApplicationPage
    {
        private CultureInfo ci = new CultureInfo("en-US");
        private double _dragDistanceToOpen = 75.0;
        private double _dragDistanceToClose = 305.0;
        private double _dragDistanceNegative = -75.0;
        private bool _isSettingsOpen = false;
        public string url = "http://mobiett.verisun.com:8181";
        private int ca = 2;
        private List<BusStopNodesModel.BusStopNodes> busNodes = new List<BusStopNodesModel.BusStopNodes>();
        private List<Models.BusStops> busstopStorage = new List<Models.BusStops>();

        private FrameworkElement _feContainer;
        public AnaEkran()
        {

            InitializeComponent();
            StoryBoardA.Begin();
            _feContainer = this.SettingsPane as FrameworkElement;
            HttpOperation.Dispatcher = Dispatcher;
            if (IsolatedStorageSettings.ApplicationSettings["LanguageCode"].ToString() != "Türkçe")
                settingsLabel.Text = "Settings";
        }

        GeoCoordinateWatcher gwatcher;

        private void myFavorites()
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains("mybusStops"))
            {
                Dictionary<string, List<BusStop>> busList = (Dictionary<string, List<Models.BusStop>>)IsolatedStorageSettings.ApplicationSettings["mybusStops"];

                int j = 0;
                foreach (var myBusS in busList[City.Current.id])
                {
                    if (myBusS != null && myBusS.name != null && !myBusS.name.Equals(""))
                    {
                        var item = new BusStopNodesModel.BusStopNodes();
                        if (j % 2 == 0)
                        {
                            item.color = "#FFF7F7F7";
                        }
                        else
                        {
                            item.color = "#FFF";
                        }
                        string _name = myBusS.name;
                        item.type = "BusStop";
                        item.node = new BusStopNodesModel.Node();
                        item.node.id = myBusS.id;
                        item.node.name = _name;
                        item.node.code = myBusS.code;
                        item.node.directionDescription = myBusS.directionDescription;
                        if (_name.Length > 60)
                        {
                            item.node.name = item.node.name.Substring(0, 60) + "...";
                        }
                        item.image = "/Mobiett;component/Images/icon_busstop@2x.png";
                        item.node.directionDescription = " " + myBusS.directionDescription + " YÖNÜ";
                        item.node.lineCode = "";
                        item.node.mrgn = new Thickness(10, 8, 0, 0);
                        j++;
                        busNodes.Add(item);
                    }
                }
            }

            if (IsolatedStorageSettings.ApplicationSettings.Contains("mybuses"))
            {
                Dictionary<string, List<BuslinesModel>> busList = (Dictionary<string, List<Models.BuslinesModel>>)IsolatedStorageSettings.ApplicationSettings["mybuses"];
                if (busList.ContainsKey(City.Current.id))
                {
                    var _color = "#FFF";
                    if (busNodes.Count != 0)
                    {
                        if (busNodes.Last().color == "#FFF")
                        {
                            _color = "#FFF7F7F7";
                        }
                    }
                    int k = 0;
                    foreach (var myBusS in busList[City.Current.id])
                    {
                        var item = new BusStopNodesModel.BusStopNodes();

                        item.color = _color;
                        string _name = myBusS.name;
                        item.type = "BusLine";
                        item.node = new BusStopNodesModel.Node();
                        item.node.id = myBusS.id;
                        item.node.name = _name;
                        item.node.code = myBusS.code;

                        item.image = "/Mobiett;component/Images/icon_bus@2x1.png";
                        item.node.lineCode = item.node.code;
                        item.node.directionDescription = "";
                        item.node.mrgn = new Thickness(10, 32, 0, 0);
                        k++;
                        busNodes.Add(item);
                        if (_color == "#FFF")
                        {
                            _color = "#FFF7F7F7";
                        }
                        else
                        {
                            _color = "#FFF";

                        }
                    }
                }
            }
            if (busNodes != null && busNodes.Count > 0)
                noFav.Visibility = Visibility.Collapsed;
            
            Dispatcher.BeginInvoke(() =>
            {
                listBox2.ItemsSource = busNodes;
                FavsHeader.Visibility = Visibility.Visible;
            });
        }

        private void ContentPanel_Loaded(object sender, RoutedEventArgs e)
        {
            HttpOperation.SendRequest<object, List<SideMenu>>(null, "/advertisement/sidemenu", "GET", callback);

            if (!IsolatedStorageSettings.ApplicationSettings.Contains("EnableLocation") || !IsolatedStorageSettings.ApplicationSettings.Contains("PrivacyPolicy"))
            {
                InitializeComponent();
                MessageBoxResult m = MessageBox.Show(LangugeResource.PopupMessage_GPSOff, LangugeResource.PopupHeader_Location, MessageBoxButton.OKCancel);
                if (m == MessageBoxResult.OK)
                {
                    Uri Uri = new Uri("/Settings.xaml", UriKind.RelativeOrAbsolute);
                    NavigationService.Navigate(Uri);
                }
            }
            else
            {
                myFavorites();
                if (IsolatedStorageSettings.ApplicationSettings.Contains("anaEkranList"))
                {
                    Dispatcher.BeginInvoke(() => listBox1.ItemsSource = this.busstopStorage);
                    Dispatcher.BeginInvoke(() => image4.Visibility = Visibility.Visible);
                    Dispatcher.BeginInvoke(() => FavsHeader.Visibility = Visibility.Visible);
                    Dispatcher.BeginInvoke(() => imageAnime.Visibility = Visibility.Collapsed);
                    this.busstopStorage = (List<Models.BusStops>)IsolatedStorageSettings.ApplicationSettings["anaEkranList"];

                }
                try
                {
                    if ((Boolean)IsolatedStorageSettings.ApplicationSettings["EnableLocation"])
                    {
                        if (gwatcher == null)
                        {
                            gwatcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
                            gwatcher.StatusChanged += new EventHandler<GeoPositionStatusChangedEventArgs>(gwatcher_StatusChanged);
                            gwatcher.PositionChanged += new EventHandler<GeoPositionChangedEventArgs<GeoCoordinate>>(gwatcher_PositionChanged);
                            gwatcher.MovementThreshold = 100;
                            gwatcher.Start();
                        }
                    }
                    else
                    {
                        Dispatcher.BeginInvoke(() => listBox1.Visibility = Visibility.Collapsed);
                        Dispatcher.BeginInvoke(() => image4.Visibility = Visibility.Visible);
                        Dispatcher.BeginInvoke(() => FavsHeader.Visibility = Visibility.Visible);
                        Dispatcher.BeginInvoke(() => imageAnime.Visibility = Visibility.Collapsed);
                    }
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                var _vBar = ((FrameworkElement)VisualTreeHelper.GetChild(ScrollViewer, 0)).FindName("VerticalScrollBar") as ScrollBar;
                _vBar.ValueChanged += _vBar_ValueChangedHandler;

            }
        }

        private void callback(List<SideMenu> arg1, Exception arg2)
        {
            foreach (var sideMenu in arg1)
            {
                switch (sideMenu.id)
                {
                    case "3":
                        Sidemenu_3Stack.Visibility = Visibility.Visible;
                        Sidemenu_3Stack.Tag = sideMenu.url;
                        Sidemenu_3Image.Source = new BitmapImage(new Uri(sideMenu.imageUrl));
                        var singleOrDefault = sideMenu.advertisementMessages.SingleOrDefault(
                            q => q.languageCode == LangugeResource.LanguageCode.ToLower());
                        Sidemenu_3Text.Text = (singleOrDefault != null) ?
                            singleOrDefault.description :
                            "Bilgilendirme Anketi";
                        break;
                }
            }
        }


        private void _vBar_ValueChangedHandler(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (e.NewValue < 0)
            {
                if (e.NewValue >= 0)
                    this.NearestHeader.SetVerticalOffset(0 - e.NewValue);
            }
            else
                this.NearestHeader.SetVerticalOffset(0 - 0);
        }

        private void GestureListener_OnDragDelta(object sender, DragDeltaGestureEventArgs e)
        {
            if (e.Direction == System.Windows.Controls.Orientation.Horizontal && e.HorizontalChange > 0 && !_isSettingsOpen)
            {
                double offset = _feContainer.GetHorizontalOffset().Value + e.HorizontalChange;
                if (offset > _dragDistanceToOpen)
                    this.OpenSettings();
                else
                    _feContainer.SetHorizontalOffset(offset);
            }
            if (e.Direction == System.Windows.Controls.Orientation.Horizontal && e.HorizontalChange < 0 && _isSettingsOpen)
            {
                double offsetContainer = _feContainer.GetHorizontalOffset().Value + e.HorizontalChange;
                if (offsetContainer < _dragDistanceToClose)
                    this.CloseSettings();
                else
                    _feContainer.SetHorizontalOffset(offsetContainer);
            }
        }

        private void GestureListener_OnDragCompleted(object sender, DragCompletedGestureEventArgs e)
        {
            if (e.Direction == System.Windows.Controls.Orientation.Horizontal && e.HorizontalChange > 0 && !_isSettingsOpen)
            {
                if (e.HorizontalChange < _dragDistanceToOpen)
                    this.ResetLayoutRoot();
                else
                    this.OpenSettings();
            }
            if (e.Direction == System.Windows.Controls.Orientation.Horizontal && e.HorizontalChange < 0 && _isSettingsOpen)
            {
                if (e.HorizontalChange > _dragDistanceNegative)
                    this.ResetLayoutRoot();
                else
                    this.CloseSettings();
            }
        }

        private void ScrollViewer_MouseMove(object sender, MouseEventArgs e)
        {
            UIElement scrollContent = (UIElement)this.ScrollViewer.Content;
            CompositeTransform ct = scrollContent.RenderTransform as CompositeTransform;
            if (ct != null && ct.TranslateY > 0)
                this.NearestHeader.SetVerticalOffset(ct.TranslateY);
        }

        private void ScrollViewer_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UIElement scrollContent = (UIElement)this.ScrollViewer.Content;
            CompositeTransform ct = scrollContent.RenderTransform as CompositeTransform;
            if (ct != null)
            {
                if (ct.TranslateY > 0)
                    this.NearestHeader.SetVerticalOffset(0);
            }
        }

        public class myJsonData { public string methodType; public string url; public Dictionary<string, string> headerMap;}

        void gwatcher_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            switch (e.Status)
            {
                case GeoPositionStatus.Disabled:
                    MessageBox.Show(LangugeResource.PopupMessage_GPSDisabled);
                    Dispatcher.BeginInvoke(() => listBox1.Visibility = Visibility.Collapsed);
                    Dispatcher.BeginInvoke(() => image4.Visibility = Visibility.Visible);
                    Dispatcher.BeginInvoke(() => FavsHeader.Visibility = Visibility.Visible);
                    Dispatcher.BeginInvoke(() => imageAnime.Visibility = Visibility.Collapsed);
                    break;

                case GeoPositionStatus.NoData:
                    MessageBox.Show(LangugeResource.PopupMessage_GPSNoData);
                    break;
            }
        }

        private void gwatcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {

            // Yakınımdaki duraklar URL'i

            double lat = 41.119;
            double lng = 29.075;
            if ((Boolean)IsolatedStorageSettings.ApplicationSettings["EnableLocation"])
            {
                lat = e.Position.Location.Latitude;
                lng = e.Position.Location.Longitude;
            }
            IsolatedStorageSettings.ApplicationSettings["lat"] = lat;
            IsolatedStorageSettings.ApplicationSettings["lng"] = lng;
            string[] stLat = lat.ToString().Replace(',', '.').Split('.');
            string newLat = stLat[0];
            int latLength = 0;
            if (stLat.Length > 1)
            {
                if (stLat[1].Length > 6)
                {
                    latLength = 6;
                }
                else
                {
                    latLength = stLat[1].Length;
                }
                newLat = stLat[0] + "." + stLat[1].Substring(0, latLength);
            }
            string[] stLng = lng.ToString().Replace(',', '.').Split('.');
            string newLng = stLng[0];
            int lngLength = 0;
            if (stLng.Length > 1)
            {
                if (stLng[1].Length > 6)
                {
                    lngLength = 6;
                }
                else
                {
                    lngLength = stLng[1].Length;
                }
                newLng = stLng[0] + "." + stLng[1].Substring(0, lngLength);
            }
            var q = string.Format("/busstops/nearest?cityId={0}&lat={1}&lon={02}", City.Current.id, newLat, newLng);
            HttpOperation.SendRequest<object, List<BusStops>>(null, q, "GET", Response_Completed);

            gwatcher.Stop();
        }

        private void Response_Completed(List<BusStops> arg1, Exception arg2)
        {
            var busstop = arg1;

            if (busstop != null)
                foreach (var item in busstop)
                {
                    item.busStop.directionDescription = item.busStop.directionDescription;
                    if (Convert.ToDouble(item.distance) < 1000)
                    {
                        string dist;
                        dist = (Convert.ToDouble(item.distance) * 1000).ToString().Substring(0, 1) + (Convert.ToDouble(item.distance) * 1000).ToString().Substring(1, 2) + " m";
                        item.distanceString = dist;
                    }
                    else
                    {
                        string dist;
                        dist = (Convert.ToDouble((item.distance))).ToString().Substring(0, 1) + (Convert.ToDouble((item.distance))).ToString().Substring(1, 2) + " km";
                        item.distanceString = dist;
                    }
                }
            busstop.Take(4).ForEach(q =>
            {
                HttpOperation.SendRequest<object, List<BusStopsLineModel.BusStopsLine>>(null, "/busstops/" + q.busStop.id + "/buses", "GET",
                    (list, exception) =>
                    {
                        if (exception != null && list == null)
                            return;
                        var buses = string.Join(",", list.Select(w => w.line.code));
                        q.county = buses;
                        Dispatcher.BeginInvoke(() => listBox1.ItemsSource = busstop.Take(4).ToList());
                    });
                //var response = HttpOperation.SendRequest<object, List<JObject>>(null, "/busstops/" + q.id + "/buses", "GET").Result;
                //var buses = string.Join(",", response.Select(w => w["line"]["code"].Value<string>()));
                //q.directionDescription = buses;
            });
            Dispatcher.BeginInvoke(() => image4.Visibility = Visibility.Visible);
            Dispatcher.BeginInvoke(() => FavsHeader.Visibility = Visibility.Visible);
            Dispatcher.BeginInvoke(() => textBlock1.Visibility = Visibility.Visible);
            //Dispatcher.BeginInvoke(() => textBlock2.Visibility = Visibility.Visible);
            IsolatedStorageSettings.ApplicationSettings["anaEkranList"] = busstop.Take(4).ToList();
            Dispatcher.BeginInvoke(() => listBox1.ItemsSource = busstop.Take(4).ToList());
            Dispatcher.BeginInvoke(() => listBox2.Visibility = Visibility.Visible);
            Dispatcher.BeginInvoke(() => imageAnime.Visibility = Visibility.Collapsed);
        }


        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                while (NavigationService.RemoveBackEntry() != null)
                {
                    NavigationService.RemoveBackEntry();
                }
            }
        }

        private void image2_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (_isSettingsOpen)
            {
                CloseSettings();
            }
            else
            {
                var trans = _feContainer.GetHorizontalOffset().Transform;
                trans.Animate(trans.X, 380, TranslateTransform.XProperty, 300, 0, new CubicEase
                {
                    EasingMode = EasingMode.EaseOut
                });
                _isSettingsOpen = true;
            }
        }

        private void textBox1_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri Uri = new Uri("/AramaEkrani.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(Uri);
        }

        private void textBox1_GotFocus(object sender, RoutedEventArgs e)
        {
            textBox1.Text = "";
            textBox1.Background = new SolidColorBrush(Colors.Transparent);
            textBox1.BorderBrush = new SolidColorBrush(Colors.Transparent);
            textBox1.Foreground = new SolidColorBrush(Colors.White);
        }

        private void listBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BusStopNodesModel.BusStopNodes m = listBox2.SelectedItem as BusStopNodesModel.BusStopNodes;

            if (m != null)
            {
                if (m.type == "BusStop")
                {
                    string _msg = m.node.name + "," + m.node.id + " , " + m.node.directionDescription + "," + m.node.code;
                    NavigationService.Navigate(new Uri("/DurakDetay.xaml?msg=" + _msg, UriKind.Relative));
                }
                else
                {
                    string _msg = m.node.code + "," + m.node.name + " , " + m.node.directionDescription + " , " + m.node.id;
                    NavigationService.Navigate(new Uri("/OtobusDetay.xaml?msg=" + _msg, UriKind.Relative));
                }
            }
        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
            {
                return;
            }
            else
            {
                BusStops m = listBox1.SelectedItem as BusStops;
                string _msg = m.busStop.name + "," + m.busStop.id + " , " + m.busStop.directionDescription + " YÖNÜ" + "," + m.busStop.code;
                NavigationService.Navigate(new Uri("/DurakDetay.xaml?msg=" + _msg, UriKind.Relative));
                listBox1.SelectedIndex = -1;
            }
        }

        private void listBox2_GotFocus(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.CurrentThread.CurrentCulture.
        }

        //SIDE MENU
        #region SideMenu

        void MoveViewWindow(double left)
        {

        }

        private void image2_ManipulationStarted(object sender, ManipulationStartedEventArgs e)
        {
            if (_isSettingsOpen)
                CloseSettings();
            else
                OpenSettings();
        }

        private void image2_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {

        }

        private void image2_ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            if (_isSettingsOpen)
                CloseSettings();
            else
                OpenSettings();
        }

        #endregion SideMenu

        private void StackPanel_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri uri = new Uri("/DurakDetay.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }

        private void image6_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AlarmList.xaml", UriKind.RelativeOrAbsolute));
        }

        private void image2_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {
            if (_isSettingsOpen)
                CloseSettings();
            else
                OpenSettings();
        }

        private void OpenSettings()
        {
            if (_isSettingsOpen)
                CloseSettings();
            var trans = _feContainer.GetHorizontalOffset().Transform;
            trans.Animate(trans.X, 380, TranslateTransform.XProperty, 300, 0, new CubicEase
            {
                EasingMode = EasingMode.EaseOut
            });
            _isSettingsOpen = true;
        }

        private void CloseSettings()
        {
            var trans = _feContainer.GetHorizontalOffset().Transform;
            trans.Animate(trans.X, 0, TranslateTransform.XProperty, 300, 0, new CubicEase
            {
                EasingMode = EasingMode.EaseOut
            });
            _isSettingsOpen = false;
        }

        private void ResetLayoutRoot()
        {
            if (!_isSettingsOpen)
                _feContainer.SetHorizontalOffset(0.0);
            else
                _feContainer.SetHorizontalOffset(380.0);
        }
        private void image1_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri uri = new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }

        private void TextBlock_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri uri = new Uri("/iettHakkindaEkrani.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }

        private void image1_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }

        private void textBlock5_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (_isSettingsOpen)
                CloseSettings();
            else
                OpenSettings();
        }

        private void StackPanel_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
        }

        private void textBlock5_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (_isSettingsOpen)
                CloseSettings();
            else
                OpenSettings();
        }


        private void QRKod_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri uri = new Uri("/QRKod.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }

        private void BeyazMasa_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri uri = new Uri("/BeyazMasa.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }

        private void Ayarlar_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri uri = new Uri("/Settings.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }


        private void SehirDegistir_Tap(object sender, RoutedEvent e)
        {
        }

        private void Map_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri uri = new Uri("/googlemaps.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }
        private void Anime_Completed(object sender, EventArgs e)
        {
            string url = "/Mobiett;component/Images/Loading/loading" + ca + ".png";
            Dispatcher.BeginInvoke(() =>
            {
                imageAnime.Source = new BitmapImage(new Uri(url, UriKind.Relative));
                if (imageAnime.Visibility == Visibility.Visible)
                {
                    ca++;
                    if (ca == 46)
                    {
                        ca = 0;
                    }
                    StoryBoardA.Begin();
                }
            });

        }

        private void Image_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

        }

        private void Image_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/RadarEkrani.xaml", UriKind.RelativeOrAbsolute));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("languageChanged"))
            {
                NavigationService.RemoveBackEntry();
            }
        }

        private void Metrobus_Tap(object sender, GestureEventArgs e)
        {
            Uri uri = new Uri("/Metrobus.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }

        private void Yardim_Tap(object sender, GestureEventArgs e)
        {
            Uri uri = new Uri("/Yardimnew.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }

        private void Duyuru_Tap(object sender, GestureEventArgs e)
        {
            Uri uri = new Uri("/Duyurular.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }

        private void Anket_Tap(object sender, GestureEventArgs e)
        {
            var sp = sender as StackPanel;
            WebBrowserTask webBrowserTask = new WebBrowserTask();

            webBrowserTask.Uri = new Uri(sp.Tag.ToString(), UriKind.Absolute);

            webBrowserTask.Show();
        }

        private void OpenContextMenu(object sender, GestureEventArgs e)
        {
            settingContextMenu.IsOpen = true;
        }

        private void DiliDegistir(object sender, RoutedEventArgs e)
        {
            ListPicker dilList = new ListPicker()
            {
                Items =
                {
                    string.Empty,
                    LangugeResource.Languages_Turkish,
                    LangugeResource.Languages_English,
                    LangugeResource.Languages_Arabic,
                    LangugeResource.Language_Deutsch,
                    LangugeResource.Language_Español,
                    LangugeResource.Language_Français,
                    LangugeResource.Language_Italiano,
                    LangugeResource.Language_Português,
                    LangugeResource.Language_pусский,
                    LangugeResource.Language_ελληνικά,
                    LangugeResource.Language_中國,
                    LangugeResource.Language_日本人
                }
            };
            dilList.Foreground = new SolidColorBrush(new Color()
            {
                A = 255,
                B = 0,
                G = 0,
                R = 0,
            });
            dilList.Background = new SolidColorBrush(new Color()
            {
                A = 255,
                B = 200,
                G = 200,
                R = 200,
            });
            dilList.SelectedItem = IsolatedStorageSettings.ApplicationSettings["LanguageCode"].ToString();
            dilList.SelectionChanged += (o, args) =>
            {
                if (args.AddedItems.Count == 0)
                    return;

                ResourceManager resourcesManager = null;
                switch (args.AddedItems[0].ToString())
                {
                    case "Türkçe":
                        resourcesManager = new System.Resources.ResourceManager("Mobiett.Resources.LangugeResource", typeof(LangugeResource).Assembly);
                        break;
                    case "English":
                        resourcesManager = new System.Resources.ResourceManager("Mobiett.Resources.LangugeResource-en", typeof(LangugeResource).Assembly);
                        break;
                    case "العربية":
                        resourcesManager = new System.Resources.ResourceManager("Mobiett.Resources.LangugeResource-ar", typeof(LangugeResource).Assembly);
                        break;
                    case "Deutsch":
                        resourcesManager = new System.Resources.ResourceManager("Mobiett.Resources.LangugeResource-de", typeof(LangugeResource).Assembly);
                        break;
                    case "ελληνικά":
                        resourcesManager = new System.Resources.ResourceManager("Mobiett.Resources.LangugeResource-el", typeof(LangugeResource).Assembly);
                        break;
                    case "Español":
                        resourcesManager = new System.Resources.ResourceManager("Mobiett.Resources.LangugeResource-es", typeof(LangugeResource).Assembly);
                        break;
                    case "Français":
                        resourcesManager = new System.Resources.ResourceManager("Mobiett.Resources.LangugeResource-fr", typeof(LangugeResource).Assembly);
                        break;
                    case "Italiano":
                        resourcesManager = new System.Resources.ResourceManager("Mobiett.Resources.LangugeResource-it", typeof(LangugeResource).Assembly);
                        break;
                    case "日本人":
                        resourcesManager = new System.Resources.ResourceManager("Mobiett.Resources.LangugeResource-ja", typeof(LangugeResource).Assembly);
                        break;
                    case "Português":
                        resourcesManager = new System.Resources.ResourceManager("Mobiett.Resources.LangugeResource-pt", typeof(LangugeResource).Assembly);
                        break;
                    case "русский":
                        resourcesManager = new System.Resources.ResourceManager("Mobiett.Resources.LangugeResource-ru", typeof(LangugeResource).Assembly);
                        break;
                    case "中國":
                        resourcesManager = new System.Resources.ResourceManager("Mobiett.Resources.LangugeResource-zh", typeof(LangugeResource).Assembly);
                        break;
                    default:
                        return;
                }
                if (LangugeResource.LanguageCode != resourcesManager.GetString("LanguageCode"))
                {
                    IsolatedStorageSettings.ApplicationSettings["LanguageCode"] = args.AddedItems[0];
                    LangugeResource.ResourceManager = resourcesManager;
                    NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.RelativeOrAbsolute));
                }
            };


            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Message = LangugeResource.PopupMessage_ChooseLanguage,
                Content = dilList,
                Background = new SolidColorBrush(new Color()
                {
                    A = 255,
                    B = 255,
                    G = 255,
                    R = 255,
                }),
                IsLeftButtonEnabled = false
            };

            #region leftButton
            //var leftButton = new Button()
            //{
            //    Content = LangugeResource.PopupButton_Cancel,
            //    Foreground = new SolidColorBrush(new Color()
            //    {
            //        A = 255,
            //        B = 0,
            //        G = 0,
            //        R = 0,
            //    }),

            //    //BorderBrush = new SolidColorBrush(new Color()
            //    //{
            //    //    A = 255,
            //    //    B = 0,
            //    //    G = 0,
            //    //    R = 0,
            //    //}),
            //    BorderThickness = new Thickness(0),
            //    Width = 180

            //};
            //leftButton.Tap += (o, args) =>
            //{
            //    messageBox.Dismiss();
            //};
            //messageBox.LeftButtonContent = leftButton;

            #endregion
            #region rightButton

            var rightButton = new Button()
            {
                Content = LangugeResource.PopupButton_Ok,
                Foreground = new SolidColorBrush(new Color()
                {
                    A = 255,
                    B = 0,
                    G = 0,
                    R = 0,
                }),
                Padding = new Thickness(0),
                //BorderBrush = new SolidColorBrush(new Color()
                //{
                //    A = 255,
                //    B = 0,
                //    G = 0,
                //    R = 0,
                //}),
                BorderThickness = new Thickness(0),
                Width = 180,

            };
            rightButton.Click += (o, args) => messageBox.Dismiss();
            messageBox.RightButtonContent = rightButton;

            #endregion

            messageBox.Foreground = new SolidColorBrush(new Color()
            {
                A = 255,
                B = 0,
                G = 0,
                R = 0,
            });
            messageBox.Show();
        }

        private void SehirDegistir(object sender, RoutedEventArgs e)
        {
            HttpOperation.SendRequest<object, List<City>>(null, "/cities", "GET", (list, exception) =>
            {
                if (exception != null || list == null)
                    return;

                ListPicker dilList = new ListPicker()
                {
                };

                foreach (var city in list)
                    dilList.Items.Add(city.name);

                dilList.Foreground = new SolidColorBrush(new Color()
                {
                    A = 255,
                    B = 0,
                    G = 0,
                    R = 0,
                });
                dilList.Background = new SolidColorBrush(new Color()
                {
                    A = 255,
                    B = 200,
                    G = 200,
                    R = 200,
                });
                dilList.SelectedItem = City.Current.name;
                dilList.SelectionChanged += (o, args) =>
                {
                    if (args.AddedItems.Count == 0)
                        return;

                    var city = list.Single(q => q.name == args.AddedItems[0].ToString());
                    IsolatedStorageSettings.ApplicationSettings[Constants.ISKeys.SelectedCity] = city;
                    City.Current = city;
                    NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.RelativeOrAbsolute));
                };


                CustomMessageBox messageBox = new CustomMessageBox()
                {
                    Message = "Change Language",
                    Content = dilList,
                    Background = new SolidColorBrush(new Color()
                    {
                        A = 255,
                        B = 255,
                        G = 255,
                        R = 255,
                    }),
                    IsLeftButtonEnabled = false
                };

                #region leftButton
                //var leftButton = new Button()
                //{
                //    Content = LangugeResource.PopupButton_Cancel,
                //    Foreground = new SolidColorBrush(new Color()
                //    {
                //        A = 255,
                //        B = 0,
                //        G = 0,
                //        R = 0,
                //    }),

                //    //BorderBrush = new SolidColorBrush(new Color()
                //    //{
                //    //    A = 255,
                //    //    B = 0,
                //    //    G = 0,
                //    //    R = 0,
                //    //}),
                //    BorderThickness = new Thickness(0),
                //    Width = 180

                //};
                //leftButton.Tap += (o, args) =>
                //{
                //    messageBox.Dismiss();
                //};
                //messageBox.LeftButtonContent = leftButton;

                #endregion
                #region rightButton

                var rightButton = new Button()
                {
                    Content = LangugeResource.PopupButton_Ok,
                    Foreground = new SolidColorBrush(new Color()
                    {
                        A = 255,
                        B = 0,
                        G = 0,
                        R = 0,
                    }),
                    Padding = new Thickness(0),
                    //BorderBrush = new SolidColorBrush(new Color()
                    //{
                    //    A = 255,
                    //    B = 0,
                    //    G = 0,
                    //    R = 0,
                    //}),
                    BorderThickness = new Thickness(0),
                    Width = 180,

                };
                rightButton.Click += (o, args) => messageBox.Dismiss();
                messageBox.RightButtonContent = rightButton;

                #endregion

                messageBox.Foreground = new SolidColorBrush(new Color()
                {
                    A = 255,
                    B = 0,
                    G = 0,
                    R = 0,
                });
                messageBox.Show();
            });
        }

        private void HowToGo_Tap(object sender, GestureEventArgs e)
        {
            WebBrowserTask webBrowserTask = new WebBrowserTask();

            webBrowserTask.Uri = new Uri("http://nasilgiderim.iett.gov.tr/", UriKind.Absolute);

            webBrowserTask.Show();
        }

        private void Twitter_Tap(object sender, GestureEventArgs e)
        {
            WebBrowserTask webBrowserTask = new WebBrowserTask();

            webBrowserTask.Uri = new Uri("https://twitter.com/ietttr", UriKind.Absolute);

            webBrowserTask.Show();
        }
    }

    #region Extensions
    public static class Extensions
    {
        public static void SetVerticalOffset(this FrameworkElement fe, double offset)
        {
            var translateTransform = fe.RenderTransform as TranslateTransform;
            if (translateTransform == null)
            {
                // create a new transform if one is not alreayd present
                var trans = new TranslateTransform()
                {
                    Y = offset
                };
                fe.RenderTransform = trans;
            }
            else
            {
                translateTransform.Y = offset;
            }
        }
    }

    public struct Offset
    {
        public double Value { get; set; }
        public TranslateTransform Transform { get; set; }
    }
    #endregion
}