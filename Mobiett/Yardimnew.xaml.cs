﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace Mobiett
{
    public partial class Yardimnew : PhoneApplicationPage
    {
        public Yardimnew()
        {
            InitializeComponent();
        }

        public static bool FirstLaunch { get; set; }

        private void button_kapat(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FirstLaunch = false;
            Uri uri = new Uri("/Anaekran.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }
    }
}