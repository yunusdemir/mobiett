﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using Microsoft.Phone.Controls;
using Newtonsoft.Json;
using Mobiett.Framework;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using Mobiett.Models;
using Mobiett.Resources;

namespace Mobiett
{
    public partial class SeferSaatleri : PhoneApplicationPage
    {
        string msg = "";
        string dataType = "";
        string _id = "";
        private string code = "";
        private string name = "";
        private string nameYon = "";
       
        private string direction = "";
        private string url = "http://159.8.34.170:8181";
   
       

        private BuslineTimetableModel.BuslineTimetable myBusList = new BuslineTimetableModel.BuslineTimetable();
        private BusStopNodesModel.Node mynode = new BusStopNodesModel.Node();

        public SeferSaatleri()
        {
            InitializeComponent();
        }

       

        public class myJsonData { public string methodType; public string url; public Dictionary<string, string> headerMap;}

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
           
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.TryGetValue("msg", out msg))
            {
                var _msgArray = msg.Split(',');
                string _code = _msgArray[0];
                this.code = _code;
                string _name = _msgArray[1];
                this.name = _name;
                this._id = _msgArray[3].Trim();
                string[] _yonnew = _name.Split('-');
                string _yon = _yonnew[_yonnew.Length - 1].Trim();
                if (_msgArray.Length > 4)
                {
                    this.direction = _msgArray[4].Trim();
                }
                else
                {
                    this.direction = "DEPARTURE";
                }
            }
            if (NavigationContext.QueryString.TryGetValue("dataType", out dataType))
            {
                if (dataType == "Batch")
                {
                    timetableShow();
                }
            }
        }

        private void timetable_Loaded()
        {
            HttpOperation.SendRequest<object>(null, "/buslines/code/" + this.code+ "/timetable?cityId="+City.Current.id, "GET", Response_Completed2);
        }

        private void Response_Completed2(string arg1, Exception arg2)
        {
            string duraklarvar = "";
            duraklarvar = arg1.ToString();
            byte[] duraklarByte = Encoding.UTF8.GetBytes(duraklarvar);
            MemoryStream duraklarStr = new MemoryStream(duraklarByte);
            duraklarStr.Position = 0;
            byte[] encryptedData = new byte[duraklarStr.Length];
            duraklarStr.Read(encryptedData, 0, (int)duraklarStr.Length);

            var result = Convert.ToBase64String(encryptedData);
            var finalReslt = Convert.FromBase64String(result);
            var str = Encoding.UTF8.GetString(finalReslt, 0, finalReslt.Length);
            DataContractJsonSerializer duraklarSerialzer = new DataContractJsonSerializer(typeof(BuslineTimetableModel.BuslineTimetable));
            BuslineTimetableModel.BuslineTimetable busLinesTimetable = (BuslineTimetableModel.BuslineTimetable)duraklarSerialzer.ReadObject(duraklarStr);

            this.nameYon = busLinesTimetable.busLine.name.Split('-')[(busLinesTimetable.busLine.name.Split('-').Length) - 1];
            Dispatcher.BeginInvoke(() => textBlock9.Text = this.name + LangugeResource.Departure_Time);

            this.myBusList = busLinesTimetable;
            var bussLisst = busLinesTimetable.lineSchedules.Where(x => x.direction == this.direction).ToList();
            foreach (var item in bussLisst)
            {
                var timeArray = item.time.Split(':');
                item.timenew = item.markCode + timeArray[0] + ":" + timeArray[1] + item.markCode;
            }
            List<BuslineTimetableModel.LineSchedule> lineSche1 = new List<BuslineTimetableModel.LineSchedule>();
            List<BuslineTimetableModel.LineSchedule> lineSche2 = new List<BuslineTimetableModel.LineSchedule>();
            List<BuslineTimetableModel.LineSchedule> lineSche3 = new List<BuslineTimetableModel.LineSchedule>();
            lineSche1 = bussLisst.Where(a => a.dayType == "WORKDAY").ToList();
            lineSche2 = bussLisst.Where(a => a.dayType == "SATURDAY").ToList();
            lineSche3 = bussLisst.Where(a => a.dayType == "SUNDAY").ToList();
            Dispatcher.BeginInvoke(() => listBox2.ItemsSource = lineSche1);
            Dispatcher.BeginInvoke(() => listBox3.ItemsSource = lineSche2);
            Dispatcher.BeginInvoke(() => listBox4.ItemsSource = lineSche3);
            var _desc = busLinesTimetable.descriptions;
            string[] arraydesc = { "", "", "", "" };
            int _j = 0;
            foreach (var item in _desc)
            {
                if (_j == 0)
                {
                    arraydesc[_j] = item.description;
                }
                else if (_j == 1)
                {
                    arraydesc[_j] = item.description;
                }
                else if (_j == 2)
                {
                    arraydesc[_j] = item.description;
                }
                else if (_j == 3)
                {
                    arraydesc[_j] = item.description;
                }
                _j++;
            }

            Dispatcher.BeginInvoke(() => run1.Text = this.code);
            Dispatcher.BeginInvoke(() => run2.Text = this.name);
            Dispatcher.BeginInvoke(() => run4.Text = arraydesc[0]);
            Dispatcher.BeginInvoke(() => run5.Text = arraydesc[1]);
            Dispatcher.BeginInvoke(() => run6.Text = arraydesc[2]);
            Dispatcher.BeginInvoke(() => run7.Text = arraydesc[3]);
            Dispatcher.BeginInvoke(() => run8.Text = busLinesTimetable.busLine.description);
            Dispatcher.BeginInvoke(() => textBlock4.Text = LangugeResource.SeferSaatleri_TravelTime + busLinesTimetable.busLine.journeyDuration + LangugeResource.Minutes);
            //Dispatcher.BeginInvoke(() => textBlock9.Text =  + LangugeResource.Departure_Time);

            //mynode.directionDescription = mynode.directionDescription + LangugeResource.Departure_Time;
        }


        private void timetableShow()
        {
            timetable_Loaded();
        }

        private void image3_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var busLinesTimetable = this.myBusList;
            if (this.direction == "DEPARTURE")
            {
                this.direction = "ARRIVAL";
                this.name = busLinesTimetable.busLine.name.Split('-')[0];
                Dispatcher.BeginInvoke(() => textBlock9.Text = this.nameYon + LangugeResource.Departure_Time);
            }
            else
            {
                this.direction = "DEPARTURE";
                this.nameYon = busLinesTimetable.busLine.name.Split('-')[(busLinesTimetable.busLine.name.Split('-').Length) - 1];
                Dispatcher.BeginInvoke(() => textBlock9.Text = this.name + LangugeResource.Departure_Time);
            }
            List<BuslineTimetableModel.LineSchedule> bussLisst = busLinesTimetable.lineSchedules.Where(x => x.direction == this.direction).ToList();
            foreach (var item in bussLisst)
            {
                var timeArray = item.time.Split(':');
                item.timenew = item.markCode + timeArray[0] + ":" + timeArray[1] + item.markCode;
            }
            
            List<BuslineTimetableModel.LineSchedule> lineSche1 = new List<BuslineTimetableModel.LineSchedule>();
            List<BuslineTimetableModel.LineSchedule> lineSche2 = new List<BuslineTimetableModel.LineSchedule>();
            List<BuslineTimetableModel.LineSchedule> lineSche3 = new List<BuslineTimetableModel.LineSchedule>();
            lineSche1 = bussLisst.Where(a => a.dayType == "WORKDAY").ToList();
            lineSche2 = bussLisst.Where(a => a.dayType == "SATURDAY").ToList();
            lineSche3 = bussLisst.Where(a => a.dayType == "SUNDAY").ToList();
            Dispatcher.BeginInvoke(() => listBox2.ItemsSource = lineSche1);
            Dispatcher.BeginInvoke(() => listBox3.ItemsSource = lineSche2);
            Dispatcher.BeginInvoke(() => listBox4.ItemsSource = lineSche3);
            var _desc = busLinesTimetable.descriptions;
            string[] arraydesc = { "", "", "", "" };
            int _j = 0;
            foreach (var item in _desc)
            {
                if (_j == 0)
                {
                    arraydesc[_j] = item.description;
                }
                else if (_j == 1)
                {
                    arraydesc[_j] = item.description;
                }
                else if (_j == 2)
                {
                    arraydesc[_j] = item.description;
                }
                else if (_j == 3)
                {
                    arraydesc[_j] = item.description;
                }
                _j++;
            }
            Dispatcher.BeginInvoke(() => run4.Text = arraydesc[0]);
            Dispatcher.BeginInvoke(() => run5.Text = arraydesc[1]);
            Dispatcher.BeginInvoke(() => run6.Text = arraydesc[2]);
            Dispatcher.BeginInvoke(() => run7.Text = arraydesc[3]);
            Dispatcher.BeginInvoke(() => run8.Text = busLinesTimetable.busLine.description);
            Dispatcher.BeginInvoke(() => textBlock4.Text = LangugeResource.SeferSaatleri_TravelTime + busLinesTimetable.busLine.journeyDuration + LangugeResource.Minutes);
            
        }

    }
}