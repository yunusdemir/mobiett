﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using Microsoft.Devices;
using System.Windows.Navigation;
using System.IO;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using com.google.zxing;
using com.google.zxing.common;
using com.google.zxing.qrcode;
using Mobiett.Framework;
using Newtonsoft.Json;
using System.Text;
using System.Runtime.Serialization.Json;
using Mobiett.Models;

namespace Mobiett
{
    public partial class QRKod : PhoneApplicationPage
    {
        private readonly DispatcherTimer _timer;
        private readonly ObservableCollection<string> _matches;

        private PhotoCameraLuminanceSource _luminance;
        private QRCodeReader _reader;
        private PhotoCamera _photoCamera;
        private string url = "http://159.8.34.170:8181";


        public QRKod()
        {
            InitializeComponent();
            _matches = new ObservableCollection<string>();
            _matchesList.ItemsSource = _matches;

            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromMilliseconds(250);
            _timer.Tick += (o, arg) => ScanPreviewBuffer();
            //DisplayResult("A3418A");
            //Dispatcher.BeginInvoke(() => DisplayResult(result.Text));

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _photoCamera = new PhotoCamera();
            _photoCamera.Initialized += OnPhotoCameraInitialized;
            _previewVideo.SetSource(_photoCamera);

            CameraButtons.ShutterKeyHalfPressed += (o, arg) => _photoCamera.Focus();

            base.OnNavigatedTo(e);
        }

        private void OnPhotoCameraInitialized(object sender, CameraOperationCompletedEventArgs e)
        {
            int width = Convert.ToInt32(_photoCamera.PreviewResolution.Width);
            int height = Convert.ToInt32(_photoCamera.PreviewResolution.Height);

            _luminance = new PhotoCameraLuminanceSource(width, height);
            _reader = new QRCodeReader();

            Dispatcher.BeginInvoke(() =>
            {
                _previewTransform.Rotation = _photoCamera.Orientation;
                _timer.Start();
            });
        }

        private void ScanPreviewBuffer()
        {
            try
            {
                Dispatcher.BeginInvoke(() => stack1.Visibility = Visibility.Collapsed);
               Dispatcher.BeginInvoke(() => textBlock1.Text = "");
                _photoCamera.GetPreviewBufferY(_luminance.PreviewBufferY);
                var binarizer = new HybridBinarizer(_luminance);
                var binBitmap = new BinaryBitmap(binarizer);
                var result = _reader.decode(binBitmap);
                Dispatcher.BeginInvoke(() => DisplayResult(result.Text));
            }
            catch
            {
            }
        }

        public class myJsonData { public string methodType; public string url; public Dictionary<string, string> headerMap;}

        private void DisplayResult(string text)
        {
             //&& text.Length < 10
            if (!_matches.Contains(text))
            {
                _matches.Add(text);
                HttpOperation.SendRequest<object>(null, "/busstops/code/" + text.ToLower()+"?cityId="+City.Current.id, "GET", Response_Completed1);
            }
        }

        private void Response_Completed1(string arg1, Exception arg2)
        {
            string duraklarvar = "";
            duraklarvar = arg1.ToString();
            byte[] duraklarByte = Encoding.UTF8.GetBytes(duraklarvar);
            MemoryStream duraklarStr = new MemoryStream(duraklarByte);
            duraklarStr.Position = 0;
            byte[] encryptedData = new byte[duraklarStr.Length];
            duraklarStr.Read(encryptedData, 0, (int)duraklarStr.Length);

            var result = Convert.ToBase64String(encryptedData);
            var finalReslt = Convert.FromBase64String(result);
            var str = Encoding.UTF8.GetString(finalReslt, 0, finalReslt.Length);
            DataContractJsonSerializer duraklarSerialzer = new DataContractJsonSerializer(typeof(BusStop));
            BusStop m = (BusStop)duraklarSerialzer.ReadObject(duraklarStr);
            string _msg = m.name + "," + m.id + " , " + m.directionDescription + " YÖNÜ";
            Dispatcher.BeginInvoke(() => NavigationService.Navigate(new Uri("/DurakDetay.xaml?msg=" + _msg, UriKind.Relative)));
        }
    }
}