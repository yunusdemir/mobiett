﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Mobiett.Framework;
using Mobiett.Models;
using Mobiett.Resources;
using Newtonsoft.Json;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace Mobiett
{
    public partial class AlarmKur : PhoneApplicationPage
    {
        public int? alarmTime { get; set; }
        public BusDirectionsModel.Stop stop { get; set; }

        public static List<BusDirectionsModel.BusDirections> BusStopList { get; set; }
        public List<string> BusStopNames { get { return BusStopList.Select(q => q.stop.name).ToList(); } }
        public static BuslinesModel Bus { get; set; }

        public AlarmKur()
        {
            InitializeComponent();
        }

        private void DurakSec(object sender, GestureEventArgs e)
        {
            lstPicker.Open();
        }

        private void UIElement_OnTap(object sender, GestureEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void SureSec(object sender, GestureEventArgs e)
        {
            txtDakika.Focus();
        }

        private void TxtDakika_OnGotFocus(object sender, RoutedEventArgs e)
        {
            if (txtDakika.Text.Contains("?"))
                txtDakika.Text = String.Empty;
            txtDakika.SelectAll();
        }

        private void TxtDakika_OnLostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtDakika.Text) || txtDakika.Text == "?")
            {
                txtDakika.Text = "?";
                alarmTime = null;
            }
            else if (int.Parse(txtDakika.Text) > 40)
            {
                txtDakika.Text = 40.ToString();
                MessageBox.Show(LangugeResource.PopupMessage_MaxMinuteValue);
            }
            alarmTime = Int32.Parse(txtDakika.Text);
        }


        private void TxtDakika_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtDakika.Text.Contains(","))
            {
                txtDakika.Text = txtDakika.Text.Replace(",", String.Empty);
                MessageBox.Show(LangugeResource.PopupMessage_JustDigits);
            }
        }

        public void BtnKaydetGuncelle_OnClick(object sender, RoutedEventArgs e)
        {
            if (!alarmTime.HasValue || stop == null)
            {
                MessageBox.Show(LangugeResource.PopupMessage_InvalidData);
                return;
            }
            var alarm = new Alarm();
            alarm.languageCode = "tr";
            alarm.alarmTime = alarmTime.Value;
            alarm.busLine = new Busline { id = Bus.id };
            alarm.busStop = new Busstop { id = stop.id };
            var deviceId = (string)IsolatedStorageSettings.ApplicationSettings["DeviceId"];
            animStart();
            HttpOperation.SendRequest<Alarm, Alarm>(alarm, String.Format("/devices/{0}/alarm", deviceId), "POST", callback);

        }

        private void callback(object arg1, Exception arg2)
        {
            AnimStop();

            if (arg2 != null)
            {
                MessageBox.Show(LangugeResource.ServiceException_ExceptionOccured);
            }
            else
            {
                MessageBox.Show(LangugeResource.PopupMessage_TimerSetted);
                NavigationService.GoBack();
            }
        }

        private void LstPicker_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var busDirection = BusStopList[lstPicker.SelectedIndex];
                stop = busDirection.stop;
                txtDurakSec.Text = stop.name;
            }
        }

        private int ca = 2;
        private void Anime_Completed(object sender, EventArgs e)
        {
            string url = "/Mobiett;component/Images/Loading/loading" + ca + ".png";
            Dispatcher.BeginInvoke(() =>
            {
                imageAnime.Source = new BitmapImage(new Uri(url, UriKind.Relative));
                if (imageAnime.Visibility == Visibility.Visible)
                {
                    ca++;
                    if (ca == 46)
                    {
                        ca = 0;
                    }
                    StoryBoardA.Begin();
                }
            });
        }

        private void animStart()
        {
            Dispatcher.BeginInvoke(() =>
            {
                loadAnim.Visibility = Visibility.Visible;
                StoryBoardA.Begin();
            });
        }

        private void AnimStop()
        {

            Dispatcher.BeginInvoke(() =>
            {
                StoryBoardA.Stop();
                loadAnim.Visibility = Visibility.Collapsed;
            });
        }
    }
}