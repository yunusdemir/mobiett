﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto;

namespace Mobiett.Framework
{
    public class CipherUtility
    {
        public static string Encrypt<T>(string value, string password, string salt)
             where T : SymmetricAlgorithm, new()
        {
            DeriveBytes rgb = new Rfc2898DeriveBytes(password, Encoding.Unicode.GetBytes(salt));

            SymmetricAlgorithm algorithm = new T();

            byte[] rgbKey = rgb.GetBytes(algorithm.KeySize >> 3);
            byte[] rgbIV = rgb.GetBytes(algorithm.BlockSize >> 3);

            ICryptoTransform transform = algorithm.CreateEncryptor(rgbKey, rgbIV);

            using (MemoryStream buffer = new MemoryStream())
            {
                using (CryptoStream stream = new CryptoStream(buffer, transform, CryptoStreamMode.Write))
                {
                    using (StreamWriter writer = new StreamWriter(stream, Encoding.Unicode))
                    {
                        writer.Write(value);
                    }
                }

                return Convert.ToBase64String(buffer.ToArray());
            }
        }

        public static string Decrypt<T>(string text, string password, string salt)
           where T : SymmetricAlgorithm, new()
        {
            DeriveBytes rgb = new Rfc2898DeriveBytes(password, Encoding.Unicode.GetBytes(salt));

            SymmetricAlgorithm algorithm = new T();

            byte[] rgbKey = rgb.GetBytes(algorithm.KeySize >> 3);
            byte[] rgbIV = rgb.GetBytes(algorithm.BlockSize >> 3);

            ICryptoTransform transform = algorithm.CreateDecryptor(rgbKey, rgbIV);

            using (MemoryStream buffer = new MemoryStream(Convert.FromBase64String(text)))
            {
                using (CryptoStream stream = new CryptoStream(buffer, transform, CryptoStreamMode.Read))
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.Unicode))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }

        public static string getEncryptedData(String jsonData)
        {

            // encryption key...
            byte[] key = Encoding.UTF8.GetBytes("12345678qwertyui");

            // AES algorthim with ECB cipher & PKCS5 padding...
            IBufferedCipher cipher = CipherUtilities.GetCipher("AES/ECB/PKCS5Padding");

            // Initialise the cipher...
            cipher.Init(true, new KeyParameter(key));
            

            // Decrypt the data and write the 'final' byte stream...
            byte[] bytes = cipher.ProcessBytes(Encoding.UTF8.GetBytes(jsonData));
            byte[] final = cipher.DoFinal();

            // Write the decrypt bytes & final to memory...
            Stream encryptedStream = new MemoryStream(bytes.Length);
            encryptedStream.Write(bytes, 0, bytes.Length);
            encryptedStream.Write(final, 0, final.Length);
            encryptedStream.Flush();

            encryptedStream.Position = 0;
            byte[] encryptedData = new byte[encryptedStream.Length];
            encryptedStream.Read(encryptedData, 0, (int)encryptedStream.Length);

            // Convert the decrypted data to a string value...
            string result = Convert.ToBase64String(encryptedData);

            return result;
        }
    }
}
