﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mobiett
{
    public class Constants
    {
        public class PushInfo
        {

            public static Uri PushChannel_URI { get; set; }

            public static string PushChannel_Name = "mobiett";

            public static Microsoft.Phone.Notification.HttpNotificationChannel pushChannel { get; set; }
        }
        public class Service
        {
            public static string BaseUrl = "https://78.186.131.245:8443";
            public static string Authorization = "Basic d2luZG93czpFNDBiMlNCSFl0YUU5N00zNGM2MQ==";
        }
        public class ISKeys
        {
            public const string SelectedCity = "SelectedCity";
            public const string FirstLaunch = "FirstLaunch";
        }
    }
}
