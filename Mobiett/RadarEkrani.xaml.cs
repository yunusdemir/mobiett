﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Runtime.Serialization.Json;
using System.Windows.Media;
using System.Windows.Shapes;
using System.IO;
using System.Text;
using System.Device;
using System.Device.Location;
using Microsoft.Devices;
using Microsoft.Devices.Sensors;
using Microsoft.Phone.Controls;
using System.Net;
using Color = System.Windows.Media.Color;
using Matrix = Microsoft.Xna.Framework.Matrix;
using Microsoft.Xna.Framework;
using AppsLah;
using System.Windows.Navigation;
using Newtonsoft.Json;
using System.IO.IsolatedStorage;
using System.Windows.Media.Imaging;
using Mobiett.Framework;
using Mobiett.Models;
using Mobiett.Resources;
using Newtonsoft.Json.Linq;
using PhoneApp1;

namespace Mobiett
{
    public partial class RadarEkrani : PhoneApplicationPage
    {
        PhotoCamera camera;
        Motion motion;
        GeoCoordinateWatcher watcher;
        Viewport viewport;
        Matrix projection;
        Matrix view;
        List<Vector3> points;
        List<UIElement> POIs;
        public string url = "http://mobiett.verisun.com:8181";

        int WCSRadius = 10;
        int Radius = 2500;

        public static GeoCoordinate CurrentPosition;

        public RadarEkrani()
        {
            InitializeComponent();
            Loaded += RadarEkrani_Loaded;
            points = new List<Vector3>();
            POIs = new List<UIElement>();
        }

        void RadarEkrani_Loaded(object sender, RoutedEventArgs e)
        {
            if (POIs == null || POIs.Count == 0)
                animStart();
        }

        public void InitViewPort()
        {
            viewport = new Viewport(0, 0, (int)ActualWidth, (int)ActualHeight);
            float aspect = viewport.AspectRatio;
            projection = Matrix.CreatePerspectiveFieldOfView(1, aspect, 1, 12);
            view = Matrix.CreateLookAt(Vector3.UnitZ, Vector3.Zero, Vector3.UnitX);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            camera = new PhotoCamera();
            ViewfinderBrush.SetSource(camera);

            if (Motion.IsSupported)
            {
                motion = new Motion();
                motion.TimeBetweenUpdates = TimeSpan.FromMilliseconds(20);
                motion.CurrentValueChanged += motion_CurrentValueChanged;
                motion.Start();
                addSignPosts();
                watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
                if (watcher.Permission == GeoPositionPermission.Granted)
                {
                    watcher.MovementThreshold = 20;
                    watcher.PositionChanged += watcher_PositionChanged;
                    watcher.Start();
                }
                else
                {
                    MessageBox.Show(LangugeResource.PopupMessage_GeoPositionNotGranted);
                    NavigationService.GoBack();
                }
            }
            else
            {
                MessageBox.Show(LangugeResource.PopupMessage_MotionAPINotSupported);
                NavigationService.GoBack();

            }
            base.OnNavigatedTo(e);
        }

        void watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            if (watcher.Status == GeoPositionStatus.Ready)
            {
                watcher.Stop();

                string[] stLat = e.Position.Location.Latitude.ToString().Replace(',', '.').Split('.');
                string newLat = stLat[0];
                int latLength = 0;
                if (stLat.Length > 1)
                {
                    if (stLat[1].Length > 6)
                    {
                        latLength = 6;
                    }
                    else
                    {
                        latLength = stLat[1].Length;
                    }
                    newLat = stLat[0] + "." + stLat[1].Substring(0, latLength);
                }
                string[] stLng = e.Position.Location.Longitude.ToString().Replace(',', '.').Split('.');
                string newLng = stLng[0];
                int lngLength = 0;
                if (stLng.Length > 1)
                {
                    if (stLng[1].Length > 6)
                    {
                        lngLength = 6;
                    }
                    else
                    {
                        lngLength = stLng[1].Length;
                    }
                    newLng = stLng[0] + "." + stLng[1].Substring(0, lngLength);
                }

                HttpOperation.SendRequest<object, List<Models.BusStops>>(null, "/busstops/nearest=" + newLat + "," + newLng + "?cityId=" + City.Current.id, "GET",
                    (arg1, arg2) =>
                    {
                        AnimStop();
                        foreach (var busStopse in arg1)
                        {
                            GeoCoordinate GeoLocation = new GeoCoordinate(busStopse.busStop.latitude, busStopse.busStop.longitude);
                            double Distance = Math.Round(GeoLocation.GetDistanceTo(e.Position.Location));
                            double Bearing = Math.Round(ARHelper.CalcBearing(GeoLocation, e.Position.Location), 0);

                            Dispatcher.BeginInvoke(() => addLabel(ARHelper.AngleToVector(Bearing, (WCSRadius * Distance / Radius)), busStopse.busStop.name, false, busStopse));
                        }
                    });

            }
        }

        void motion_CurrentValueChanged(object sender, SensorReadingEventArgs<MotionReading> e)
        {
            Dispatcher.BeginInvoke(() => CurrentValueChanged(e.SensorReading));
        }
        void CurrentValueChanged(MotionReading reading)
        {
            if (viewport == null || viewport.Width == 0)
                InitViewPort();

            Matrix attitude = Matrix.CreateRotationX(MathHelper.PiOver2) * reading.Attitude.RotationMatrix;
            for (int i = 0; i < points.Count; i++)
            {
                Matrix world = Matrix.CreateWorld(points[i], Vector3.UnitZ, Vector3.UnitX);
                Vector3 projected = viewport.Project(Vector3.Zero, projection, view, world * attitude);
                if (projected.Z > 1 || projected.Z < 0)
                {
                    POIs[i].Visibility = Visibility.Collapsed;
                }
                else
                {
                    POIs[i].Visibility = Visibility.Visible;
                    TranslateTransform tt = new TranslateTransform();
                    tt.X = projected.X - (POIs[i].RenderSize.Width / 2);
                    tt.Y = projected.Y - (POIs[i].RenderSize.Height / 2);
                    POIs[i].RenderTransform = tt;
                }
            }
        }

        void addSignPosts()
        {
            //addLabel(ARHelper.AngleToVector(0, WCSRadius), "N", true);
            //addLabel(ARHelper.AngleToVector(45, WCSRadius), "NE", true);
            //addLabel(ARHelper.AngleToVector(90, WCSRadius), "E", true);
            //addLabel(ARHelper.AngleToVector(180, WCSRadius), "S", true);
            //addLabel(ARHelper.AngleToVector(270, WCSRadius), "W", true);
        }

        void addLabel(Vector3 position, string name, bool isSign, BusStops busStopse)
        {
            //TextBlock tb = new TextBlock();
            //Border b = new Border();
            //b.Child = tb;
            //tb.Text = name;
            //b.HorizontalAlignment = HorizontalAlignment.Left;
            //b.VerticalAlignment = VerticalAlignment.Top;
            //tb.FontSize = 30;
            //tb.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            //b.Background = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            //if (isSign)
            //{
            //    tb.FontSize = 25;
            //    tb.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
            //    b.Background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            //}
            //b.SetValue(Canvas.ZIndexProperty, 2);
            //b.Visibility = Visibility.Collapsed;
            var arPin = new ARPin(name);
            arPin.Tag = busStopse;
            arPin.Margin = new Thickness(0, -160, 0, 0);
            arPin.Tap += arPin_Tap;
            LayoutRoot.Children.Add(arPin);
            points.Add(position);
            POIs.Add(arPin);
        }

        void arPin_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var arPin = sender as ARPin;
            var m = arPin.Tag as BusStops;
            string _msg = m.busStop.name + "," + m.busStop.id + " , " + m.busStop.directionDescription;
            NavigationService.Navigate(new Uri("/DurakDetay.xaml?msg=" + _msg, UriKind.Relative));
        }

        private int ca = 2;
        private void Anime_Completed(object sender, EventArgs e)
        {
            string url = "/Mobiett;component/Images/Loading/loading" + ca + ".png";
            Dispatcher.BeginInvoke(() =>
            {
                imageAnime.Source = new BitmapImage(new Uri(url, UriKind.Relative));
                if (imageAnime.Visibility == Visibility.Visible)
                {
                    ca++;
                    if (ca == 46)
                    {
                        ca = 0;
                    }
                    StoryBoardA.Begin();
                }
            });
        }

        private void animStart()
        {
            Dispatcher.BeginInvoke(() =>
            {
                loadAnim.Visibility = Visibility.Visible;
                StoryBoardA.Begin();
            });
        }

        private void AnimStop()
        {

            Dispatcher.BeginInvoke(() =>
            {
                StoryBoardA.Stop();
                loadAnim.Visibility = Visibility.Collapsed;
            });
        }
    }
}