﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using Microsoft.Phone.Controls;
using System.Device.Location;
using Mobiett.Models;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using System.IO;
using Mobiett.Framework;
using System.Text;
using System.Runtime.Serialization.Json;
using Microsoft.Phone.Controls.Maps;
using System.Xml;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using Mobiett.Resources;

namespace Mobiett
{
    public partial class HatBilgisi : PhoneApplicationPage
    {
        GeoCoordinateWatcher watcher;
        string _id = "";
        string msg = "";
        string _name = "";
        private double _lat = 0;
        private double _lng = 0;
        bool firstSetViewBool;
        public string url = "http://159.8.34.170:8181";
        //private List<BusDirectionsModel.Stop> _busStop = new List<BusDirectionsModel.Stop>();
        private BuslinesModel mybus = new BuslinesModel();
        private List<BusDirectionsModel.BusDirections> buslineStopDep = new List<BusDirectionsModel.BusDirections>();
        private List<BusDirectionsModel.BusDirections> buslineStopAr = new List<BusDirectionsModel.BusDirections>();
        private ObservableCollection<PinData> _pinsA = new ObservableCollection<PinData>();
        public ObservableCollection<PinData> PinsA { get { return this._pinsA; } }
        PushpinViewModela _viewModel;
        ContextMenu contextMenum;

        public HatBilgisi()
        {
            firstSetViewBool = false;
            InitializeComponent();
            //getBusClass();
            if ((Boolean)IsolatedStorageSettings.ApplicationSettings["EnableLocation"])
            {
                if (watcher == null)
                {
                    watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.Default);
                    watcher.MovementThreshold = 20;
                    watcher.StatusChanged += new EventHandler<GeoPositionStatusChangedEventArgs>(watcher_StatusChanged);
                    // watcher.PositionChanged += new EventHandler<GeoPositionChangedEventArgs<GeoCoordinate>>(watcher_PositionChanged);

                }
                watcher.Start();
            }
            StoryBoard2.Begin();
        }

        private void DoubleAnimation_Completed(object sender, EventArgs e)
        {
            getBusClass(msg);
            StoryBoard2.Begin();
        }

        void watcher_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            switch (e.Status)
            {
                case GeoPositionStatus.Disabled:
                    MessageBox.Show(LangugeResource.PopupMessage_GPSDisabled);
                    break;

                case GeoPositionStatus.NoData:
                    MessageBox.Show(LangugeResource.PopupMessage_GPSNoData);
                    break;
            }
        }

        void watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            if (e.Position.Location.IsUnknown)
            {
                MessageBox.Show(LangugeResource.PopupMessage_WaitForLocation);
                return;
            }

            this._lat = 41.119;
            this._lng = 29.075;
            if ((Boolean)IsolatedStorageSettings.ApplicationSettings["EnableLocation"])
            {
                this._lat = e.Position.Location.Latitude;
                this._lng = e.Position.Location.Longitude;
            }
        }



        private void getBusClass(string _id)
        {
            HttpOperation.SendRequest<object>(null, "/buslines/" + msg + "/buses/direction=DEPARTURE?cityId="+City.Current.id, "GET", Response_Completed1);
        }

        private void Response_Completed1(string arg1, Exception arg2)
        {
            string duraklarvar = "";
            duraklarvar = arg1;
            byte[] duraklarByte = Encoding.UTF8.GetBytes(duraklarvar);
            MemoryStream duraklarStr = new MemoryStream(duraklarByte);
            duraklarStr.Position = 0;
            byte[] encryptedData = new byte[duraklarStr.Length];
            duraklarStr.Read(encryptedData, 0, (int)duraklarStr.Length);

            var result = Convert.ToBase64String(encryptedData);
            var finalReslt = Convert.FromBase64String(result);
            var str = Encoding.UTF8.GetString(finalReslt, 0, finalReslt.Length);
            DataContractJsonSerializer duraklarSerialzer = new DataContractJsonSerializer(typeof(List<BusDirectionsModel.BusDirections>));
            List<BusDirectionsModel.BusDirections> busLinesStops = (List<BusDirectionsModel.BusDirections>)duraklarSerialzer.ReadObject(duraklarStr);
            this.buslineStopDep = busLinesStops;
            //setMyMapa(this.buslineStopDep);

            _viewModel = new PushpinViewModela(this.buslineStopDep, this._name);
            Dispatcher.BeginInvoke(() => mapPins.ItemsSource = _viewModel.Pushpins);
            if (!firstSetViewBool)
            {
                Dispatcher.BeginInvoke(() => this.lineMap.SetView(new GeoCoordinate(41, 29), 12.0));
                firstSetViewBool = true;
            }
        }


        public class PinData { public GeoCoordinate PinLocation { get; set; } public string Description { get; set; } public string Name { get; set; } public DateTime DatetimeAdded { get; set; } }

        private void Pushpin_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

            _viewModel = _viewModel != null ? _viewModel : new PushpinViewModela(this.buslineStopDep, this._name);
            var _ppmodel = sender as Pushpin;
            ContextMenu contextMenu = ContextMenuService.GetContextMenu(_ppmodel);
            this.contextMenum = contextMenu;
            contextMenu.DataContext = _viewModel.Pushpins.Where
                (c => (c.Id
                    == _ppmodel.Tag.ToString())).FirstOrDefault();
            if (contextMenu.Parent == null)
            {
                contextMenu.IsOpen = true;
            }

        }


        private void GestureListener_Tap_1(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {

        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.TryGetValue("msg", out msg))
            {
                getBusClass(msg);
            }
            if (NavigationContext.QueryString.TryGetValue("name", out _name))
            {
            }
        }



    }
    public class PushpinViewModela
    {
        private ObservableCollection<PushpinModel> _pushpins =
                                   new ObservableCollection<PushpinModel>();

        public ObservableCollection<PushpinModel> Pushpins
        {
            get
            {
                return _pushpins;
            }
        }

        public PushpinViewModela(List<BusDirectionsModel.BusDirections> busStops, string name)
        {
            InitializePushpins(busStops, name);
        }

        private void InitializePushpins(List<BusDirectionsModel.BusDirections> busStoplar, string name)
        {
            foreach (var item in busStoplar)
            {
                _pushpins.Add(new PushpinModel
                {
                    Description = "" + item.stop.directionDescription + " YÖNÜ",
                    Name = item.stop.name.Length > 33 ? item.stop.name.Substring(0, 30) + "..." : item.stop.name,
                    Id = "" + item.stop.id,
                    Location = new GeoCoordinate(item.stop.latitude, item.stop.longitude),
                    Icon = new Uri("/Mobiett;component/Images/icn_durak_pin2@2x.png", UriKind.Relative),
                    InsideIcon = "/Mobiett;component/Images/icon_busstop@2x.png"

                });
            }
            foreach (var item in busStoplar)
            {
                foreach (var jitem in item.buses)
                {
                    _pushpins.Add(new PushpinModel
                    {
                        Description = "" + jitem.directionDescription + " YÖNÜ",
                        Name = name.Length > 33 ? name.Substring(0, 30) + "..." : name,
                        Id = "" + jitem.doorNo,
                        Location = new GeoCoordinate(jitem.busLocation.latitude, jitem.busLocation.longitude),
                        Icon = new Uri("/Mobiett;component/Images/icn_bus_location@2x.png", UriKind.Relative),
                        InsideIcon = "/Mobiett;component/Images/icon_bus@2x1.png"
                    });
                }
            }
        }
    }



    public class myJsonData { public string methodType; public string url; public Dictionary<string, string> headerMap;
    public string body { get; set; }
    }


    public enum GoogleTilesTypes
    {
        Hybrid,
        Physical,
        Street,
        Satellite,
        WaterOverlay
    }

    public class GoogleTiles : Microsoft.Phone.Controls.Maps.TileSource
    {
        private int _server;
        private char _mapmode;
        private GoogleTileTypes _tiletypes;

        public GoogleTileTypes TileTypes
        {
            get { return _tiletypes; }
            set
            {
                _tiletypes = value;
                MapMode = MapModeConverter(value);
            }
        }

        public char MapMode
        {
            get { return _mapmode; }
            set { _mapmode = value; }
        }

        public int Server
        {
            get { return _server; }
            set { _server = value; }
        }

        public GoogleTiles()
        {
            UriFormat = @"http://mt{0}.google.com/vt/lyrs={1}&z={2}&x={3}&y={4}";
            Server = 0;
        }

        public override Uri GetUri(int x, int y, int zoomLevel)
        {
            if (zoomLevel > 0)
            {
                var Url = string.Format(UriFormat, Server, MapMode, zoomLevel, x, y);
                return new Uri(Url);
            }
            return null;
        }

        private char MapModeConverter(GoogleTileTypes tiletype)
        {
            switch (tiletype)
            {
                case GoogleTileTypes.Hybrid:
                    {
                        return 'y';
                    }
                case GoogleTileTypes.Physical:
                    {
                        return 't';
                    }
                case GoogleTileTypes.Satellite:
                    {
                        return 's';
                    }
                case GoogleTileTypes.Street:
                    {
                        return 'm';
                    }
                case GoogleTileTypes.WaterOverlay:
                    {
                        return 'r';
                    }
            }
            return ' ';
        }

    }

}

