﻿using GalaSoft.MvvmLight;
using Mobiett.Resources;
using System.ComponentModel;
using Wp7nl.Globalization;

namespace Mobiett.ViewModel
{
    
    public class MainViewModel : ViewModelBase
    {

        Wp7nl.Globalization.LanguageSettingsViewModel a = new LanguageSettingsViewModel();


        public MainViewModel()
        {
            // Note: en-US is added by default
            a.AddLanguages(new Language { Description = "Deutsch", Locale = "de-DE" });
            a.AddLanguages(new Language { Description = "Nederlands", Locale = "nl-NL" });


            //AddLanguages(new Language { Description = "Arabic", Locale = "ar-AR" });
            PropertyChanged += MainViewModelPropertyChanged;
        }

        void MainViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "CurrentLanguage")
            {
                a.SetLanguageFromCurrentLocale();
                //LocalizedStrings.LocalizedStringsResource.UpdateLanguage();
            }
        }

        
    }
}