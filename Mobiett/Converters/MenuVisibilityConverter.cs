﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Mobiett.Models;

namespace Mobiett.Converters
{
    public class MenuVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return City.Current.code == "IST" ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
