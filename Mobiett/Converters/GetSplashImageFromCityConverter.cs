﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Mobiett.Models;

namespace Mobiett.Converters
{
    public class GetSplashImageFromCityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var city = value as City;
            return city.staticFiles.Single(q => q.screenName == "splash").url;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
