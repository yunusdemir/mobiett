﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using Microsoft.Phone.Controls;
using Newtonsoft.Json;
using Mobiett.Framework;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using Mobiett.Models;
using System.IO.IsolatedStorage;
using System.Device.Location;
using Microsoft.Phone.Tasks;

namespace Mobiett
{
    public partial class Settings : PhoneApplicationPage
    {
        public Settings()
        {
            InitializeComponent();
            if (IsolatedStorageSettings.ApplicationSettings.Contains("EnableLocation"))
            {
                if ((Boolean)IsolatedStorageSettings.ApplicationSettings["EnableLocation"])
                {
                    EnableLocation.IsChecked = true;
                }
                else
                {
                    EnableLocation.IsChecked = false;
                }
            }
            else
            {
                EnableLocation.IsChecked = false;
            }
            if (IsolatedStorageSettings.ApplicationSettings.Contains("PrivacyPolicy"))
            {
                if ((Boolean)IsolatedStorageSettings.ApplicationSettings["PrivacyPolicy"])
                {
                    EnablePrivacy.IsChecked = true;
                }
                else
                {
                    EnablePrivacy.IsChecked = false;
                }
            }
            else
            {
                EnableLocation.IsChecked = false;
            }
        }

        private void textBlock2_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            new EmailComposeTask
            {
                Subject = "Gizlilik Sorusu",
                Body = "Mobiett",
                To = ""
            }.Show();
        }

        private void buttonBack_Click(object sender, RoutedEventArgs e)
        {
            if (!IsolatedStorageSettings.ApplicationSettings.Contains("EnableLocation"))
            {
                IsolatedStorageSettings.ApplicationSettings["EnableLocation"] = false;
            }
            if (!IsolatedStorageSettings.ApplicationSettings.Contains("PrivacyPolicy"))
            {
                IsolatedStorageSettings.ApplicationSettings["PrivacyPolicy"] = false;
            }
            Uri Uri = new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(Uri);
        }

        private void EnablePrivacy_Checked(object sender, RoutedEventArgs e)
        {
            IsolatedStorageSettings.ApplicationSettings["PrivacyPolicy"] = true;
        }

        private void EnablePrivacy_Unchecked(object sender, RoutedEventArgs e)
        {
            IsolatedStorageSettings.ApplicationSettings["PrivacyPolicy"] = false;
        }

        private void EnableLocation_Checked(object sender, RoutedEventArgs e)
        {
            IsolatedStorageSettings.ApplicationSettings["EnableLocation"] = true;
        }

        private void EnableLocation_Unchecked(object sender, RoutedEventArgs e)
        {
            IsolatedStorageSettings.ApplicationSettings["EnableLocation"] = false;
        }

    }
}