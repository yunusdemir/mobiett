﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mobiett
{
    public class Init
    {
        public Init.Version version { get; set; }
        public string token { get; set; }

        public class Version
        {
            public string code { get; set; }
        }
    }
}
