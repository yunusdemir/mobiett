﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Device.Location;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Mobiett.Framework;
using Mobiett.Models;
using Mobiett.Resources;
using Newtonsoft.Json;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace Mobiett
{
    public partial class AlarmDurakSec : PhoneApplicationPage
    {
        string msg = "";
        string dataType = "";
        string _id = "";
        private string code = "";
        private string name = "";
        private string nameYon = "";
        private string direction = "DEPARTURE";
        private string url = "http://159.8.34.170:8181";
        private BuslinesModel mybus = new BuslinesModel();
        private List<BusDirectionsModel.BusDirections> buslineStopDep = new List<BusDirectionsModel.BusDirections>();
        private List<BusDirectionsModel.BusDirections> buslineStopAr = new List<BusDirectionsModel.BusDirections>();
        public Uri uri = new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute);
        int itemIndex = 0;
        int itemIndexAr = 0;
        public AlarmDurakSec()
        {
            InitializeComponent();
            StoryBoard1.Begin();
            //timetable_Loaded();
            if (AddAlarm.line != null)
            {
                if (AddAlarm.line.directions.Count == 1)
                    image4.Visibility = Visibility.Collapsed;
            }
        }



        public class myJsonData { public string methodType; public string url; public Dictionary<string, string> headerMap;}

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(this._id))
            {
                getBusClass(this._id);
            }
            else
            {
                if (NavigationContext.QueryString.TryGetValue("msg", out msg))
                {
                    var _msgArray = msg.Split(',');
                    string _code = _msgArray[0];
                    this.code = _code;
                    string _name = _msgArray[1];
                    this.name = _name;
                    this._id = _msgArray[3].Trim();
                    string[] _yonnew = _name.Split('-');
                    string _yon = _yonnew[_yonnew.Length - 1].Trim();

                    this.nameYon = mybus.name.Split('-')[(mybus.name.Split('-').Length) - 1];
                    getBusClass(this._id);
                }
            }

            HttpOperation.SendRequest<object>(null, "/buslines/" + _id + "/buses/direction=DEPARTURE?cityId=" + City.Current.id, "GET", Response_Completed1);
        }

        private void Response_Completed1(string arg1, Exception arg2)
        {
            string duraklarvar = "";
            duraklarvar = arg1.ToString();
            byte[] duraklarByte = Encoding.UTF8.GetBytes(duraklarvar);
            MemoryStream duraklarStr = new MemoryStream(duraklarByte);
            duraklarStr.Position = 0;
            byte[] encryptedData = new byte[duraklarStr.Length];
            duraklarStr.Read(encryptedData, 0, (int)duraklarStr.Length);

            var result = Convert.ToBase64String(encryptedData);
            var finalReslt = Convert.FromBase64String(result);
            var str = Encoding.UTF8.GetString(finalReslt, 0, finalReslt.Length);
            DataContractJsonSerializer duraklarSerialzer = new DataContractJsonSerializer(typeof(List<BusDirectionsModel.BusDirections>));
            List<BusDirectionsModel.BusDirections> busLinesStops = (List<BusDirectionsModel.BusDirections>)duraklarSerialzer.ReadObject(duraklarStr);
            int j = 0;
            var _item = new BusDirectionsModel.BusDirections();
            double latitude = 0;
            double longitude = 0;

            if (IsolatedStorageSettings.ApplicationSettings.Contains("lat") && IsolatedStorageSettings.ApplicationSettings.Contains("lng"))
            {
                latitude = (double)IsolatedStorageSettings.ApplicationSettings["lat"];
                longitude = (double)IsolatedStorageSettings.ApplicationSettings["lng"];
                var coord = new GeoCoordinate(latitude, longitude);
                var nearest = busLinesStops.Count != 0 ? busLinesStops.Select(x => new GeoCoordinate(x.stop.latitude, x.stop.longitude))
                                       .OrderBy(x => x.GetDistanceTo(coord))
                                       .FirstOrDefault() : null;
                foreach (var item in busLinesStops)
                {
                    if (nearest != null && item.stop.latitude == nearest.Latitude && item.stop.longitude == nearest.Longitude)
                    {
                        item.stop.youAreHere = LangugeResource.Inform_YouAreHere;
                        item.stop.youAreHereImage = "/Mobiett;component/Images/userlocation@2x.png";
                        this.itemIndex = j;
                    }
                    j++;
                }
            }
            foreach (var item in busLinesStops)
            {
                if (item.buses.Count > 0)
                {
                    if (busLinesStops.IndexOf(item) <= this.itemIndex)
                    {
                        item.busImage = "/Mobiett;component/Images/icon_wherethebusis@2x.png";
                    }
                    else
                    {
                        item.busImage = "/Mobiett;component/Images/icon_passive_bus@2x.png";
                    }
                }
                else
                {
                    item.busImage = "";
                }

            }
            this.buslineStopDep = busLinesStops;
            Dispatcher.BeginInvoke(() => listBox1.ItemsSource = busLinesStops);
            Dispatcher.BeginInvoke(() => listBox1.SelectedIndex = this.itemIndex);
            Dispatcher.BeginInvoke(() => progressBar.Visibility = Visibility.Collapsed);
            //Dispatcher.BeginInvoke(() => imgfav.Visibility = Visibility.Visible);

            getBusLineArrive();
        }


        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            //e.Cancel = true;
            var lastPage = NavigationService.BackStack.FirstOrDefault();
            var _uri = new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute);
            if (lastPage != null && lastPage.Source.Equals(_uri))
            {
                this.uri = lastPage.Source;
                NavigationService.Navigate(this.uri);
            }
            else
            {
            }
        }

        private void getBusLineArrive()
        {
            HttpOperation.SendRequest<object>(null, "/buslines/" + this._id + "/buses/direction=ARRIVAL?city" + City.Current.id, "GET", Response_Completed6);
        }

        private void Response_Completed6(string arg1, Exception arg2)
        {
            string duraklarvar = "";
            duraklarvar = arg1.ToString();
            byte[] duraklarByte = Encoding.UTF8.GetBytes(duraklarvar);
            MemoryStream duraklarStr = new MemoryStream(duraklarByte);
            duraklarStr.Position = 0;
            byte[] encryptedData = new byte[duraklarStr.Length];
            duraklarStr.Read(encryptedData, 0, (int)duraklarStr.Length);

            var result = Convert.ToBase64String(encryptedData);
            var finalReslt = Convert.FromBase64String(result);
            var str = Encoding.UTF8.GetString(finalReslt, 0, finalReslt.Length);
            DataContractJsonSerializer duraklarSerialzer = new DataContractJsonSerializer(typeof(List<BusDirectionsModel.BusDirections>));
            List<BusDirectionsModel.BusDirections> busLinesStops = (List<BusDirectionsModel.BusDirections>)duraklarSerialzer.ReadObject(duraklarStr);
            int j = 0;
            this.buslineStopAr = busLinesStops;
            var _item = new BusDirectionsModel.BusDirections();
            double latitude = 0;
            double longitude = 0;
            if (IsolatedStorageSettings.ApplicationSettings.Contains("lat") && IsolatedStorageSettings.ApplicationSettings.Contains("lng"))
            {
                latitude = (double)IsolatedStorageSettings.ApplicationSettings["lat"];
                longitude = (double)IsolatedStorageSettings.ApplicationSettings["lng"];
                var coord = new GeoCoordinate(latitude, longitude);
                var nearest = busLinesStops.Count != 0 ? busLinesStops.Select(x => new GeoCoordinate(x.stop.latitude, x.stop.longitude))
                                       .OrderBy(x => x.GetDistanceTo(coord))
                                       .FirstOrDefault() : null;
                foreach (var item in busLinesStops)
                {
                    if (nearest != null && item.stop.latitude == nearest.Latitude && item.stop.longitude == nearest.Longitude)
                    {
                        item.stop.youAreHere = LangugeResource.Inform_YouAreHere;
                        item.stop.youAreHereImage = "/Mobiett;component/Images/userlocation@2x.png";
                        this.itemIndexAr = j;
                    }
                    j++;
                }
            }
            foreach (var item in busLinesStops)
            {
                if (item.buses.Count > 0)
                {
                    if (busLinesStops.IndexOf(item) <= this.itemIndex)
                    {
                        item.busImage = "/Mobiett;component/Images/icon_wherethebusis@2x.png";
                    }
                    else
                    {
                        item.busImage = "/Mobiett;component/Images/icon_passive_bus@2x.png";
                    }
                }
                else
                {
                    item.busImage = "";
                }

            }
        }

        private void getBusClass(string _id)
        {
            HttpOperation.SendRequest<object>(null, "/buslines/" + _id + "?cityId=" + City.Current.id, "GET", Response_Completed4);
        }

        private void Response_Completed4(string arg1, Exception arg2)
        {
            string otobusvar = "";
            otobusvar = arg1.ToString();
            byte[] duraklarByte = Encoding.UTF8.GetBytes(otobusvar);
            MemoryStream duraklarStr = new MemoryStream(duraklarByte);
            duraklarStr.Position = 0;
            byte[] encryptedData = new byte[duraklarStr.Length];
            duraklarStr.Read(encryptedData, 0, (int)duraklarStr.Length);

            var result = Convert.ToBase64String(encryptedData);
            var finalReslt = Convert.FromBase64String(result);
            var str = Encoding.UTF8.GetString(finalReslt, 0, finalReslt.Length);
            DataContractJsonSerializer duraklarSerialzer = new DataContractJsonSerializer(typeof(BuslinesModel));
            BuslinesModel m = (BuslinesModel)duraklarSerialzer.ReadObject(duraklarStr);
            this.mybus = m;
        }

        private void image3_Tap(object sender, GestureEventArgs e)
        {
            Uri uri = new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.TryGetValue("msg", out msg))
            {
                var _msgArray = msg.Split(',');
                this.msg = msg;
                string _code = _msgArray[0];
                this.code = _code;
                string _name = _msgArray[1];
                this.name = _name;
                this._id = _msgArray[3].Trim();
                string[] _yonnew = _name.Split('-');
                string _yon = _yonnew[_yonnew.Length - 1].Trim();

                this.nameYon = this.name.Split('-')[(this.name.Split('-').Length) - 1];
                // this.nameYon = this.name.Split('-')[0];
                //Dispatcher.BeginInvoke(() => textBlock3.Text = this.nameYon + " YÖNÜ");

            }
            if (NavigationContext.QueryString.TryGetValue("dataType", out dataType))
            {
                if (dataType == "Batch")
                {
                    timetableShow();
                }
            }
        }

        private void DoubleAnimation_Completed_1(object sender, EventArgs e)
        {

            StoryBoard2.Begin();
        }

        private void DoubleAnimation_Completed(object sender, EventArgs e)
        {
            StoryBoard1.Begin();

        }

        private void timetable_Loaded()
        {
            HttpOperation.SendRequest<object>(null, "/buslines/" + this._id + "/timetable?cityId=" + City.Current.id, "GET", Response_Completed2);
        }

        private void Response_Completed2(string arg1, Exception arg2)
        {
            string duraklarvar = "";
            duraklarvar = arg1.ToString();
            byte[] duraklarByte = Encoding.UTF8.GetBytes(duraklarvar);
            MemoryStream duraklarStr = new MemoryStream(duraklarByte);
            duraklarStr.Position = 0;
            byte[] encryptedData = new byte[duraklarStr.Length];
            duraklarStr.Read(encryptedData, 0, (int)duraklarStr.Length);

            var result = Convert.ToBase64String(encryptedData);
            var finalReslt = Convert.FromBase64String(result);
            var str = Encoding.UTF8.GetString(finalReslt, 0, finalReslt.Length);
            DataContractJsonSerializer duraklarSerialzer = new DataContractJsonSerializer(typeof(BuslineTimetableModel.BuslineTimetable));
            BuslineTimetableModel.BuslineTimetable busLinesTimetable = (BuslineTimetableModel.BuslineTimetable)duraklarSerialzer.ReadObject(duraklarStr);
            foreach (var item in busLinesTimetable.lineSchedules)
            {
                var timeArray = item.time.Split(':');
                item.time = timeArray[0] + ":" + timeArray[1];
            }
            List<BuslineTimetableModel.LineSchedule> lineSche1 = new List<BuslineTimetableModel.LineSchedule>();
            List<BuslineTimetableModel.LineSchedule> lineSche2 = new List<BuslineTimetableModel.LineSchedule>();
            List<BuslineTimetableModel.LineSchedule> lineSche3 = new List<BuslineTimetableModel.LineSchedule>();
        }

        private void image6_Tap(object sender, GestureEventArgs e)
        {
            string _msg = this.msg + " , " + this.direction;
            NavigationService.Navigate(new Uri("/SeferSaatleri.xaml?msg=" + _msg + "&dataType=" + "Batch", UriKind.Relative));
            //timetableShow();
        }

        private void timetableShow()
        {
            timetable_Loaded();
        }

        private void image5_Tap(object sender, GestureEventArgs e)
        {
            Uri uri = new Uri("/HatBilgisi.xaml?msg=" + this._id + "&name=" + this.code + " " + this.name, UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }

        private void listBox1_Loaded(object sender, RoutedEventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                Dispatcher.BeginInvoke(() => listBox1.ScrollIntoView(listBox1.Items[listBox1.SelectedIndex]));
            }
        }

        private void imgfav_Tap(object sender, GestureEventArgs e)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains("mybuses"))
            {
                var busList = (Dictionary<string, List<BuslinesModel>>)IsolatedStorageSettings.ApplicationSettings["mybuses"];
                if (!busList.ContainsKey(City.Current.id))
                    busList.Add(City.Current.id, new List<BuslinesModel> { mybus });
                else
                    busList[City.Current.id].Add(this.mybus);
                IsolatedStorageSettings.ApplicationSettings["mybuses"] = busList;
            }
            else
            {
                IsolatedStorageSettings.ApplicationSettings["mybuses"] = new Dictionary<string, List<BuslinesModel>>();
                var busList = (Dictionary<string, List<BuslinesModel>>)IsolatedStorageSettings.ApplicationSettings["mybuses"];
                if (!busList.ContainsKey(City.Current.id))
                    busList.Add(City.Current.id, new List<BuslinesModel> { mybus });
                else
                    busList[City.Current.id].Add(this.mybus);
                IsolatedStorageSettings.ApplicationSettings["mybuses"] = busList;
            }

            MessageBox.Show(LangugeResource.PopupMessage_AddToFav);
        }

        private void imageStar_Tap(object sender, GestureEventArgs e)
        {
            var busList = (Dictionary<string, List<BuslinesModel>>)IsolatedStorageSettings.ApplicationSettings["mybuses"];
            busList[City.Current.id].RemoveAll(q => q.id == mybus.id);
            MessageBox.Show(LangugeResource.PopupMessage_RemoveFromFav);
        }

        private void image4_Tap(object sender, GestureEventArgs e)
        {
            var bLineDirection = this.mybus;
            if (this.direction == "DEPARTURE")
            {
                this.direction = "ARRIVAL";
                this.name = bLineDirection.name.Split('-')[0];
                List<BusDirectionsModel.BusDirections> mybusDirect = new List<BusDirectionsModel.BusDirections>();
                Dispatcher.BeginInvoke(() => listBox1.ItemsSource = this.buslineStopAr);
                if (this.buslineStopAr.Count != 0)
                {
                    int arIndex = this.itemIndexAr >= this.buslineStopAr.Count ? this.itemIndexAr - 1 : this.itemIndexAr;
                    Dispatcher.BeginInvoke(() => listBox1.SelectedIndex = arIndex);
                }
            }

            else
            {
                this.direction = "DEPARTURE";
                this.nameYon = bLineDirection.name.Split('-')[(bLineDirection.name.Split('-').Length) - 1];
                Dispatcher.BeginInvoke(() => listBox1.ItemsSource = this.buslineStopDep);
                if (this.buslineStopDep.Count != 0)
                {
                    int depIndex = this.itemIndex >= this.buslineStopDep.Count ? this.itemIndex - 1 : this.itemIndex;
                    Dispatcher.BeginInvoke(() => listBox1.SelectedIndex = depIndex);
                }
            }
        }

        private void image7_Tap(object sender, GestureEventArgs e)
        {
            Uri uri = new Uri("/AlarmKur.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }

        private void ListBox1_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ListBox1_OnTap(object sender, GestureEventArgs e)
        {
            var busDirections = listBox1.SelectedItems[0] as BusDirectionsModel.BusDirections;
            AddAlarm.stop = busDirections.stop;
            AddAlarm.stop.directionDescription = direction;
            NavigationService.GoBack();
        }
    }
}