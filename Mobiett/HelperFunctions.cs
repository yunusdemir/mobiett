﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
namespace Mobiett
{
    public static class HelperFunctions
    {
        public static string GetApplicationVersion()
        {
            string appVersion = XDocument.Load("WMAppManifest.xml")
                .Root.Element("App").Attribute("Version").Value;
            return appVersion;
        }
        public static void SetHeaders(this HttpWebRequest req)
        {
            req.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString();
            req.Headers["Authorization"] = "Basic d2luZG93czpFNDBiMlNCSFl0YUU5N00zNGM2MQ==";
            req.ContentType = "application/json";
        }
    }
}
