﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using System.Device.Location;
using Microsoft.Phone.Controls.Maps;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using Mobiett.Models;
using Mobiett.Framework;
using Newtonsoft.Json;
using System.Xml;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using Microsoft.Phone.Controls.Maps.Platform;
using System.IO.IsolatedStorage;
using Mobiett.Resources;

namespace Mobiett
{

    public partial class googlemaps : PhoneApplicationPage
    {
        GeoCoordinateWatcher watcher;
        // static int centerControl = 0;
        private double _lat;
        private double _lng;
        public string url;
        private List<BusStops> _busStop;
        private GeoCoordinate MyLoc;
        private GeoCoordinate MyPoint;
        PushpinViewModel _viewModel;
        ContextMenu contextMenum;
        bool getNearBool;
        bool firstSetViewBool;
        bool myPoinBool;
        public Uri uri;

        public googlemaps()
        {
            uri = new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute);
            url = "http://159.8.34.170:8181";
            _busStop = new List<BusStops>();
            getNearBool = false;
            myPoinBool = false;
            firstSetViewBool = false;
            _lat = 0;
            _lng = 0;
            InitializeComponent();
        }

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            if ((Boolean)IsolatedStorageSettings.ApplicationSettings["EnableLocation"])
            {
                if (watcher == null)
                {
                    watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.Default);
                    watcher.MovementThreshold = 20;
                    watcher.StatusChanged += new EventHandler<GeoPositionStatusChangedEventArgs>(watcher_StatusChanged);
                    watcher.PositionChanged += new EventHandler<GeoPositionChangedEventArgs<GeoCoordinate>>(watcher_PositionChanged);
                    watcher.Start();
                }
            }
            else
            {
                MessageBoxResult m = MessageBox.Show(LangugeResource.PopupMessage_GPSOff, LangugeResource.PopupHeader_Location, MessageBoxButton.OKCancel);
                if (m == MessageBoxResult.OK)
                {
                    Uri Uri = new Uri("/Settings.xaml", UriKind.RelativeOrAbsolute);
                    NavigationService.Navigate(Uri);
                }
            }
        }

        void watcher_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            switch (e.Status)
            {
                case GeoPositionStatus.Disabled:
                    MessageBox.Show(LangugeResource.PopupMessage_GPSOff);
                    break;

                case GeoPositionStatus.NoData:
                    MessageBox.Show(LangugeResource.PopupMessage_GPSNoData);
                    break;
            }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            //e.Cancel = true;
            var lastPage = NavigationService.BackStack.FirstOrDefault();
            var _uri = new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute);
            if (lastPage != null && lastPage.Source.Equals(_uri))
            {
                this.uri = lastPage.Source;
                NavigationService.Navigate(this.uri);
            }
            else
            {
            }
        }

        void watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            if (e.Position.Location.IsUnknown)
            {
                MessageBox.Show(LangugeResource.PopupMessage_GPSOff);
                return;
            }

            this._lat = 41.119;
            this._lng = 29.075;
            if ((Boolean)IsolatedStorageSettings.ApplicationSettings["EnableLocation"])
            {
                this._lat = e.Position.Location.Latitude;
                this._lng = e.Position.Location.Longitude;
            }
            this.MyLoc = new GeoCoordinate(this._lat, this._lng);
            if (!firstSetViewBool)
            {
                Dispatcher.BeginInvoke(() => this.myMap.SetView(this.MyLoc, 16.0));
                firstSetViewBool = true;
            }
            if (!myPoinBool)
            {
                this.MyPoint = new GeoCoordinate(this._lat, this._lng);
                Pushpin pin = new Pushpin();
                pin.Location = this.MyPoint;
                pin.Foreground = new SolidColorBrush(Colors.Blue);
                pin.Background = new SolidColorBrush(Colors.Red);
                pin.BorderBrush = new SolidColorBrush(Colors.White);
                pin.BorderThickness = new Thickness(1);
                this.myMap.Children.Add(pin);
                myPoinBool = true;
                myMap.SetView(this.MyPoint, 16);
            }

            LocationRect bounds = myMap.BoundingRectangle;
            Location northwest = bounds.Northwest;
            Location northeast = bounds.Northeast;
            Location southwest = bounds.Southwest;
            Location southeast = bounds.Southeast;
            //this.MyLoc = new GeoCoordinate(41, 29);
            if (!this.getNearBool)
            {
                this.getNearBool = true;
                getNearestTwo(northwest, northeast, southwest, southeast);
            }
            //getNearest(this._lat, this._lng);
        }

        public class myJsonData { public string methodType; public string url; public Dictionary<string, string> headerMap;}

        void Pushpin_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //var _uri = new Uri("/Mobiett;component/Images/bubble@2x.png", UriKind.Relative);
            //var imb = new ImageBrush();
            //imb.ImageSource = new BitmapImage(_uri);
            _viewModel = _viewModel != null ? _viewModel : new PushpinViewModel(this._busStop);
            var _ppmodel = sender as Pushpin;
            ContextMenu contextMenu = ContextMenuService.GetContextMenu(_ppmodel);
            this.contextMenum = contextMenu;
            contextMenu.DataContext = _viewModel.Pushpins.Where
                (c => (c.Location
                    == _ppmodel.Location)).FirstOrDefault();
            if (contextMenu.Parent == null)
            {
                contextMenu.IsOpen = true;
            }
        }

        public class PinData { public GeoCoordinate PinLocation { get; set; } public string Description { get; set; } public string Name { get; set; } public DateTime DatetimeAdded { get; set; } }

        private void GestureListener_Tap(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            Border border = sender as Border;
            ContextMenu contextMenu = ContextMenuService.GetContextMenu(border);
            Pushpin pin = sender as Pushpin;
            var myTem = _viewModel.Pushpins.Where(c => (c.Id.Equals(border.Tag))).FirstOrDefault();
            //contextMenu.DataContext = _viewModel.Pushpins.Where(c => (c.Id.Equals(border.Tag))).FirstOrDefault();
            string _msg = myTem.Name + "," + myTem.Id + " , " + myTem.Yon;
            if (this.contextMenum != null)
            {
                this.contextMenum.IsOpen = false;
            }
            NavigationService.Navigate(new Uri("/DurakDetay.xaml?msg=" + _msg, UriKind.Relative));


        }

        private void getNearestTwo(GeoCoordinate tLeft, GeoCoordinate tRight, GeoCoordinate bLeft, GeoCoordinate bRight)
        {
            // Yakınımdaki duraklar URL'i
            string stLeftLat = tLeft.Latitude.ToString().Replace(',', '.');
            string stLeftLng = tLeft.Longitude.ToString().Replace(',', '.');
            string stRightLat = tRight.Latitude.ToString().Replace(',', '.');
            string stRightLng = tRight.Longitude.ToString().Replace(',', '.');
            string sbLeftLat = bLeft.Latitude.ToString().Replace(',', '.');
            string sbLeftLng = bLeft.Longitude.ToString().Replace(',', '.');
            string sbRightLat = bRight.Latitude.ToString().Replace(',', '.');
            string sbRightLng = bRight.Longitude.ToString().Replace(',', '.');

            HttpOperation.SendRequest<object, List<BusStops>>(null, "/busstops/borders=" + stLeftLat + "," + stLeftLng + "," + stRightLat + "," + stRightLng + "," + sbLeftLat + "," + sbLeftLng + "," + sbRightLat + "," + sbRightLng, "GET", Response_Completed2);

        }

        private void Response_Completed2(List<BusStops> arg1, Exception arg2)
        {
            var busstop=arg1;
            this._busStop = busstop;
            if (this._busStop.Count != 0)
            {
                foreach (var item in busstop)
                {
                    var sCoord = new GeoCoordinate(item.latitude, item.longitude);
                    var eCoord = new GeoCoordinate(this.MyPoint.Latitude, this.MyPoint.Longitude);
                    //var eCoord = new GeoCoordinate(41, 29);

                    item.distance = sCoord.GetDistanceTo(eCoord);
                    if (item.distance < 1000)
                    {
                        string[] arDist = item.distance.ToString().Split(',');
                        string dist;
                        dist = arDist[0] + " m";
                        item.distanceString = dist;
                    }
                    else
                    {
                        string dist;
                        string[] arDist = (item.distance / 1000).ToString().Split(',');
                        int arLength = arDist.Length;
                        if (arLength < 2)
                        {
                            dist = arDist[0] + " km";
                        }
                        else
                        {
                            int _length = (arDist[1].Length - 1) < 2 ? 0 : 2;
                            dist = arDist[0] + "." + arDist[1].Substring(0, _length) + " km";
                        }
                        //dist = _length == 0 ? arDist[0] + " km" : arDist[0] + "." + arDist[1].Substring(0, _length) + " km";
                        item.distanceString = dist;
                    }
                }

                _viewModel = new PushpinViewModel(this._busStop);
                Dispatcher.BeginInvoke(() => mapPins.ItemsSource = _viewModel.Pushpins);
                Dispatcher.BeginInvoke(() =>
                {

                    double _zoomLev = this.myMap.ZoomLevel < 16.0 ? 16.0 : this.myMap.ZoomLevel;
                    //this.MyLoc = this.myMap.Center;
                    //this.myMap.SetView(this.MyLoc, _zoomLev);
                    this.myMap.ZoomLevel = _zoomLev;

                });
            }
            //_viewModel = new PushpinViewModel(busstop);
            this.getNearBool = false;
        }

        
        private void myMap_MapPan(object sender, MapDragEventArgs e)
        {
            Map myMap = (Map)sender;
            double _zoomlev = myMap.ZoomLevel;
            if (!this.getNearBool && _zoomlev >= 15)
            {
                this.getNearBool = true;
                LocationRect bounds = myMap.BoundingRectangle;
                Location northwest = bounds.Northwest;
                Location northeast = bounds.Northeast;
                Location southwest = bounds.Southwest;
                Location southeast = bounds.Southeast;
                this.MyLoc = bounds.Center;
                getNearestTwo(northwest, northeast, southwest, southeast);
            }
        }

        private void myMap_MapZoom(object sender, MapZoomEventArgs e)
        {
            Map myMap = (Map)sender;
            double _zoomlev = myMap.ZoomLevel;
            if (!this.getNearBool && _zoomlev >= 15)
            {
                this.getNearBool = true;
                LocationRect bounds = myMap.BoundingRectangle;
                Location northwest = bounds.Northwest;
                Location northeast = bounds.Northeast;
                Location southwest = bounds.Southwest;
                Location southeast = bounds.Southeast;
                this.MyLoc = bounds.Center;
                getNearestTwo(northwest, northeast, southwest, southeast);
            }
        }

       

    }

    public class PushpinViewModel
    {
        private ObservableCollection<PushpinModel> _pushpins = new ObservableCollection<PushpinModel>();

        public ObservableCollection<PushpinModel> Pushpins
        {
            get
            {
                return _pushpins;
            }
        }

        public PushpinViewModel(List<BusStops> busStoplar)
        {
            InitializePushpins(busStoplar);
        }

        private void InitializePushpins(List<BusStops> busStoplar)
        {
            foreach (var item in busStoplar)
            {
                _pushpins.Add(new PushpinModel
            {
                Description = "" + item.distanceString,
                Name = "" + item.name,
                Location = new GeoCoordinate(item.latitude, item.longitude),
                DatetimeAdded = DateTime.Now,
                Id = item.id,
                Yon = item.directionDescription + " YÖNÜ",
                Icon = new Uri("/Mobiett;component/Images/icn_durak_pin2@2x.png", UriKind.Relative),
                InsideIcon = "/Mobiett;component/Images/icon_busstop@2x.png",
                BgCon = new Uri("/Mobiett;component/Images/bubble@2x.png", UriKind.Relative)
            });
            }
        }
    }

    public class PushpinModel : INotifyPropertyChanged
    {
        private GeoCoordinate _location;
        private DateTime _datetimeAdded;
        private String _description;
        private String _name;
        private Uri _icon;
        private String _insideIcon;
        private Uri _bgCon;
        private String _id;
        private String _yon;

        public GeoCoordinate Location
        {
            get
            {
                return _location;
            }
            set
            {
                if (_location != value)
                {
                    _location = value;
                    NotifyPropertyChanged("Location");
                }
            }
        }

        public Uri BgCon
        {
            get
            {
                return _bgCon;
            }
            set
            {
                if (_bgCon != value)
                {
                    _bgCon = value;
                    NotifyPropertyChanged("BgCon");
                }
            }
        }

        public DateTime DatetimeAdded
        {
            get
            {
                return _datetimeAdded;
            }
            set
            {
                if (_datetimeAdded != value)
                {
                    _datetimeAdded = value;
                    NotifyPropertyChanged("DatetimeAdded");
                }
            }
        }

        public String Description
        {
            get
            {
                return _description;
            }
            set
            {
                if (_description != value)
                {
                    _description = value;
                    NotifyPropertyChanged("Description");
                }
            }
        }

        public String Yon
        {
            get
            {
                return _yon;
            }
            set
            {
                if (_yon != value)
                {
                    _yon = value;
                    NotifyPropertyChanged("Yon");
                }
            }
        }

        public String Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    NotifyPropertyChanged("Id");
                }
            }
        }

        public Uri Icon
        {
            get
            {
                return _icon;
            }
            set
            {
                if (_icon != value)
                {
                    _icon = value;
                    NotifyPropertyChanged("Icon");
                }
            }
        }
        public String InsideIcon
        {
            get
            {
                return _insideIcon;
            }
            set
            {
                if (_insideIcon != value)
                {
                    _insideIcon = value;
                    NotifyPropertyChanged("InsideIcon");
                }
            }
        }

        public String Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }

    public enum GoogleTileTypes
    {
        Hybrid,
        Physical,
        Street,
        Satellite,
        WaterOverlay
    }

    public class GoogleTile : Microsoft.Phone.Controls.Maps.TileSource
    {
        private int _server;
        private char _mapmode;
        private GoogleTileTypes _tiletypes;

        public GoogleTileTypes TileTypes
        {
            get { return _tiletypes; }
            set
            {
                _tiletypes = value;
                MapMode = MapModeConverter(value);
            }
        }

        public char MapMode
        {
            get { return _mapmode; }
            set { _mapmode = value; }
        }

        public int Server
        {
            get { return _server; }
            set { _server = value; }
        }

        public GoogleTile()
        {
            UriFormat = @"http://mt{0}.google.com/vt/lyrs={1}&z={2}&x={3}&y={4}";
            Server = 0;
        }

        public override Uri GetUri(int x, int y, int zoomLevel)
        {
            if (zoomLevel > 0)
            {
                var Url = string.Format(UriFormat, Server, MapMode, zoomLevel, x, y);
                return new Uri(Url);
            }
            return null;
        }

        private char MapModeConverter(GoogleTileTypes tiletype)
        {
            switch (tiletype)
            {
                case GoogleTileTypes.Hybrid:
                    {
                        return 'y';
                    }
                case GoogleTileTypes.Physical:
                    {
                        return 't';
                    }
                case GoogleTileTypes.Satellite:
                    {
                        return 's';
                    }
                case GoogleTileTypes.Street:
                    {
                        return 'm';
                    }
                case GoogleTileTypes.WaterOverlay:
                    {
                        return 'r';
                    }
            }
            return ' ';
        }

    }
}