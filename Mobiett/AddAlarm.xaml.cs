﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Mobiett.Models;
using Mobiett.Resources;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace Mobiett
{
    public partial class AddAlarm : PhoneApplicationPage
    {
        private int ca = 2;
        public static string id { get; set; }
        public static BusStopNodesModel.Node line { get; set; }
        public static BusDirectionsModel.Stop stop { get; set; }
        public static string days { get; set; }
        public static int? alarmTime { get; set; }
        public static DateTime? startTime { get; set; }
        public static DateTime? endTime { get; set; }
        public static bool isActive { get; set; }

        public AddAlarm()
        {
            InitializeComponent();
        }

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            if (popup.Visibility == Visibility.Visible)
            {
                popup.Visibility = Visibility.Collapsed;
                ButtonBase_OnClick(null, null);
                e.Cancel = true;
                return;
            }

            if (MessageBox.Show(LangugeResource.PopupMessage_LeavingPage, LangugeResource.PopUpHeader_Caution, MessageBoxButton.OKCancel) != MessageBoxResult.OK)
            {
                e.Cancel = true;
                return;
            }
            id = null;
            line = null;
            stop = null;
            days = String.Empty;
            alarmTime = null;
            startTime = null;
            endTime = null;
            base.OnBackKeyPress(e);

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            txtHatSec.Text = line != null ? line.name : LangugeResource.AddAlarm_ChooseLine;
            txtDurakSec.Text = stop != null ? stop.name : LangugeResource.AddAlarm_ChooseStop;
            txtDakika.Text = alarmTime.HasValue ? alarmTime.ToString() : "?";
            txtGunSec.Text = SetDaysText();
            if (startTime != null)
            {
                baslangic.Value = startTime;
                startTime = null;
            }
            if (endTime != null)
            {
                bitis.Value = endTime;
                endTime = null;
            }
            else
                bitis.Value = bitis.Value.Value.AddHours(1);

            if (line != null && stop != null)
                txtDakika.Focus();

            if (string.IsNullOrEmpty(id))
            {
                btnKaydetGuncelle.Content = LangugeResource.AddAlarm_SaveButton;
                btnSil.Visibility = Visibility.Collapsed;
            }
            else
            {
                btnKaydetGuncelle.Content = LangugeResource.AddAlarm_UpdateButton;
                btnSil.Visibility = Visibility.Visible;
            }
        }



        private string SetDaysText()
        {
            if (string.IsNullOrEmpty(days))
                return "Gün Seç";
            var sBuilder = new StringBuilder();
            if (days.Equals("XXXXX__"))
                return "Haftaiçi";

            if (days.Equals("_____XX"))
                return "HaftaSonu";

            for (var i = 0; i < days.Length; i++)
            {
                if (days[i] == 'X')
                {
                    sBuilder.Append(((TextBlock)((CheckBox)lstGunler.Items[i]).Content).Text + ",");
                    ((CheckBox)lstGunler.Items[i]).IsChecked = true;
                }
            }
            return sBuilder.ToString();
        }

        private void HatSec(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AlarmHatSec.xaml?type=line", UriKind.RelativeOrAbsolute));
        }
        private void DurakSec(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (line == null)
            {
                MessageBox.Show(LangugeResource.PopupMessage_ChooseLine);
                return;
            }
            string _msg = line.code + "," + line.name + " , " + line.directionDescription + " , " + line.id;
            NavigationService.Navigate(new Uri("/AlarmDurakSec.xaml?msg=" + _msg, UriKind.Relative));

            //NavigationService.Navigate(new Uri("/AlarmHatSec.xaml?type=stop", UriKind.RelativeOrAbsolute));
        }
        private void SureSec(object sender, System.Windows.Input.GestureEventArgs e)
        {
            txtDakika.SelectAll();
        }
        private void GunSec(object sender, System.Windows.Input.GestureEventArgs e)
        {
            popup.Visibility = Visibility.Visible;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            popup.Visibility = Visibility.Collapsed;
            txtGunSec.Text = String.Empty;
            var gunler = new StringBuilder();
            foreach (CheckBox checkBox in lstGunler.Items)
            {
                if (checkBox.IsChecked.HasValue && checkBox.IsChecked.Value)
                {
                    gunler.Append("X");
                    txtGunSec.Text += (checkBox.Content as TextBlock).Text + ", ";
                }
                else
                    gunler.Append("_");
            }


            if (gunler.ToString().Equals("XXXXX__"))
                txtGunSec.Text = LangugeResource.WeekDays;
            else if (gunler.ToString().Equals("_____XX"))
                txtGunSec.Text = LangugeResource.WeekEnd;


            if (string.IsNullOrEmpty(txtGunSec.Text))
                txtGunSec.Text = LangugeResource.AddAlarm_ChooseDate;

            days = gunler.ToString();
        }

        private void TxtDakika_OnGotFocus(object sender, RoutedEventArgs e)
        {
            if (txtDakika.Text.Contains("?"))
                txtDakika.Text = String.Empty;
            txtDakika.SelectAll();
        }

        private void TxtDakika_OnLostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtDakika.Text))
            {
                txtDakika.Text = "?";
                alarmTime = null;
            }
            else if (int.Parse(txtDakika.Text) > 40)
            {
                txtDakika.Text = 40.ToString();
                MessageBox.Show(LangugeResource.PopupMessage_MaxMinuteValue);
            }
            alarmTime = Int32.Parse(txtDakika.Text);
        }

        private void TxtDakika_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtDakika.Text.Contains(","))
            {
                txtDakika.Text = txtDakika.Text.Replace(",", String.Empty);
                MessageBox.Show(LangugeResource.PopupMessage_JustDigits);
            }
        }

        private void BtnKaydet_OnClick(object sender, RoutedEventArgs e)
        {
            if (line == null || stop == null || !days.Contains("X") || !alarmTime.HasValue || !baslangic.Value.HasValue || !bitis.Value.HasValue)
            {
                MessageBox.Show(LangugeResource.PopupMessage_InvalidData);
                return;
            }
            if (baslangic.Value > bitis.Value || baslangic.Value.ToString() == bitis.Value.ToString())
            {
                MessageBox.Show(LangugeResource.PopupMessage_StartTimeBiggerThanEndTime);
                return;
            }
            var alarm = new Alarm
            {
                isActive = string.IsNullOrEmpty(id) || isActive,
                alarmTime = Convert.ToInt32(txtDakika.Text),
                busLine = new Busline { id = line.id, code = line.code },
                busStop = new Busstop { id = stop.id, code = stop.code },
                direction = line.directions.First(),
                endTime = bitis.Value.Value.ToString("hh:mm:ss"),
                startTime = baslangic.Value.Value.ToString("hh:mm:ss"),
                languageCode = "tr",
                repeatedDays = days,
                id = id
            };
            var deviceId = IsolatedStorageSettings.ApplicationSettings["DeviceId"];
            string url;
            string method;
            if (string.IsNullOrEmpty(id))
            {
                url = String.Format("/devices/{0}/advancedalarm", deviceId);
                method = "POST";

            }
            else
            {
                url = String.Format("/devices/{0}/advancedalarm/{1}", deviceId, id);
                method = "PUT";
            }
            animStart();
            HttpOperation.SendRequest<Alarm, object>(alarm, url, method, callback);

        }

        private void callback(object arg1, Exception arg2)
        {
            AnimStop();
            if (arg2 != null)
            {
                MessageBox.Show(LangugeResource.ServiceException_ExceptionOccured);
            }
            else
            {
                if (string.IsNullOrEmpty(id))
                {
                    MessageBox.Show(LangugeResource.PopupMessage_TimerSetted);
                }
                else
                {
                    MessageBox.Show(LangugeResource.PopupMessage_TimerUpdated);
                }
                NavigationService.GoBack();
            }
        }

        private void UIElement_OnTap(object sender, GestureEventArgs e)
        {
            txtDakika.Focus();
        }

        private void BtnSil_OnClick(object sender, RoutedEventArgs e)
        {
            var deviceId = IsolatedStorageSettings.ApplicationSettings["DeviceId"];
            var url = String.Format("/devices/{0}/advancedalarm/{1}", deviceId, id);
            var method = "DELETE";
            animStart();
            HttpOperation.SendRequest<Alarm, object>(null, url, method, callback);
        }

        private void BtnBackButton_OnTap(object sender, GestureEventArgs e)
        {
            if (MessageBox.Show(LangugeResource.PopupMessage_LeavingPage, LangugeResource.PopUpHeader_Caution, MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;
            id = null;
            line = null;
            stop = null;
            days = String.Empty;
            alarmTime = null;
            startTime = null;
            endTime = null;
            NavigationService.GoBack();
        }

        private void CheckBox_tap(object sender, GestureEventArgs e)
        {
            e.Handled = true;
        }
        private void Anime_Completed(object sender, EventArgs e)
        {
            string url = "/Mobiett;component/Images/Loading/loading" + ca + ".png";
            Dispatcher.BeginInvoke(() =>
            {
                imageAnime.Source = new BitmapImage(new Uri(url, UriKind.Relative));
                if (imageAnime.Visibility == Visibility.Visible)
                {
                    ca++;
                    if (ca == 46)
                    {
                        ca = 0;
                    }
                    StoryBoardA.Begin();
                }
            });
        }

        private void animStart()
        {
            Dispatcher.BeginInvoke(() =>
            {
                loadAnim.Visibility = Visibility.Visible;
                StoryBoardA.Begin();
            });
        }

        private void AnimStop()
        {

            Dispatcher.BeginInvoke(() =>
            {
                StoryBoardA.Stop();
                loadAnim.Visibility = Visibility.Collapsed;
            });
        }
    }
}