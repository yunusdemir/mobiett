﻿using Mobiett.Framework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Net.Http;

namespace Mobiett
{
    public static class HttpOperation
    {
        public static Dispatcher Dispatcher;
        //public static string baseUrl = "http://api-mobiett.verisun.com:8181";
        //public static string baseUrl = "http://78.186.131.245:8181";
        public static string baseUrl = "http://mobiett.verisun.com:8181";
        public static string serviceUrl = "https://api-mobiett.verisun.com:8443/mobiett/rest";
        //public static string serviceUrl = "https://localhost:8543/mobiett/rest";
        public class myJsonData
        {
            public string methodType;
            public string url;
            public Dictionary<string, string> headerMap;
            public string body;
        }
        public static void SendRequest<T, TV>(T request, string url, string method, Action<TV, Exception> callback)
        {
            var jsonReqStr = new myJsonData();
            jsonReqStr.url = serviceUrl + url;
            jsonReqStr.body = request != null ? JsonConvert.SerializeObject(request) : String.Empty;
            jsonReqStr.methodType = method;
            jsonReqStr.headerMap = new Dictionary<string, string>();
            jsonReqStr.headerMap.Add("Authorization", "Basic d2luZG93czpFNDBiMlNCSFl0YUU5N00zNGM2MQ==");
            jsonReqStr.headerMap.Add("Content-Type", "application/json");
            string sessionToken;
            if (IsolatedStorageSettings.ApplicationSettings.TryGetValue("sessionToken", out sessionToken))
            {
                jsonReqStr.headerMap.Add("sessionToken", sessionToken);
            }
            string jsonReq = JsonConvert.SerializeObject(jsonReqStr);
            Debug.WriteLine("Dec :" + jsonReq);
            string jsonResult = CipherUtility.getEncryptedData(jsonReq);
            //string jsonResult = CipherUtility.Encrypt<AesManaged>(jsonReq, "verisun.2014", "0123456789123456");
            Debug.WriteLine("Enc :" + jsonResult);

            var endUrl = baseUrl + "/crypto/rest/decryption";

            //Eklediğimiz "req.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString();" kodu responsu refresh etmek içindi
            HttpWebRequest req = HttpWebRequest.CreateHttp(endUrl);
            if (req.Headers == null) { req.Headers = new WebHeaderCollection(); }
            req.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString();
            req.Method = "GET";
            req.Headers["Authorization"] = jsonResult;
            req.BeginGetResponse((ar) =>
            {
                try
                {
                    HttpWebRequest _req = (HttpWebRequest)ar.AsyncState;
                    HttpWebResponse response = (HttpWebResponse)_req.EndGetResponse(ar);
                    using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        var _res = streamReader.ReadToEnd();
                        Debug.WriteLine("Res :" + _res);
                        var model = JsonConvert.DeserializeObject<TV>(_res);
                        Dispatcher.BeginInvoke(() =>
                        {
                            try
                            {
                                callback(model, null);
                            }
                            catch (Exception exception)
                            {
                                Console.WriteLine(exception);
                            }
                        });
                    }
                }
                catch (WebException e)
                {
                    // BUGÜN DÜZENLEDİĞİM

                    // get the HTTP_STATUS
                    int statusCode = -1;

                    if (e.Response != null)
                    {
                        //try to cast to HttpWebResponse (it should be!)

                        HttpWebResponse aResp = e.Response as HttpWebResponse;

                        if (aResp != null)
                        {
                            statusCode = (int)aResp.StatusCode;
                            if (statusCode == 401)
                            {
                                Dispatcher.BeginInvoke(() => { MainPage.Navigate("/MainPage.xaml"); });
                            }
                        }
                        else
                        {
                            // not an HttpWebResponse!

                        }

                    }
                    // Need to handle the exception some other way 
                    Dispatcher.BeginInvoke(() => callback(default(TV), new Exception("Bir Hata Oluştu.",e)));

                }
            }, req);
        }

        public static void SendRequest<T>(T request, string url, string method, Action<string, Exception> callback)
        {
            try
            {
                var jsonReqStr = new myJsonData();
                jsonReqStr.url = serviceUrl + url;
                jsonReqStr.body = request != null ? JsonConvert.SerializeObject(request) : String.Empty;
                jsonReqStr.methodType = method;
                jsonReqStr.headerMap = new Dictionary<string, string>();
                jsonReqStr.headerMap.Add("Authorization", "Basic d2luZG93czpFNDBiMlNCSFl0YUU5N00zNGM2MQ==");
                jsonReqStr.headerMap.Add("Content-Type", "application/json");
                string sessionToken;
                if (IsolatedStorageSettings.ApplicationSettings.TryGetValue("sessionToken", out sessionToken))
                {
                    jsonReqStr.headerMap.Add("sessionToken", sessionToken);
                }
                string jsonReq = JsonConvert.SerializeObject(jsonReqStr);
                Debug.WriteLine(jsonReq);
                string jsonResult = CipherUtility.getEncryptedData(jsonReq);
                //string jsonResult = CipherUtility.Encrypt<AesManaged>(jsonReq, "verisun.2014", "0123456789123456");

                var endUrl = baseUrl + "/crypto/rest/decryption";

                //Eklediğimiz "req.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString();" kodu responsu refresh etmek içindi
                HttpWebRequest req = HttpWebRequest.CreateHttp(endUrl);
                if (req.Headers == null) { req.Headers = new WebHeaderCollection(); }
                req.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString();
                req.Method = "GET";
                req.Headers["Authorization"] = jsonResult;
                req.BeginGetResponse((ar) =>
                {
                    try
                    {
                        HttpWebRequest _req = (HttpWebRequest)ar.AsyncState;
                        HttpWebResponse response = (HttpWebResponse)_req.EndGetResponse(ar);
                        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                        {

                            var _res = streamReader.ReadToEnd();
                            Debug.WriteLine(_res);
                            Dispatcher.BeginInvoke(() =>
                            {
                                try
                                {
                                    callback(_res, null);
                                }
                                catch (Exception exception)
                                {
                                    Console.WriteLine(exception);
                                }
                            });
                        }
                    }
                    catch (WebException e)
                    {
                        // BUGÜN DÜZENLEDİĞİM

                        // get the HTTP_STATUS
                        int statusCode = -1;

                        if (e.Response != null)
                        {
                            //try to cast to HttpWebResponse (it should be!)

                            HttpWebResponse aResp = e.Response as HttpWebResponse;

                            if (aResp != null)
                            {
                                statusCode = (int)aResp.StatusCode;
                                if (statusCode == 401)
                                {
                                    Dispatcher.BeginInvoke(() => { MainPage.Navigate("/MainPage.xaml"); });
                                }
                            }
                            else
                            {
                                // not an HttpWebResponse!

                            }

                        }
                        // Need to handle the exception some other way 
                        Dispatcher.BeginInvoke(() => callback(string.Empty, new Exception("Bir Hata Oluştu.")));

                    }
                }, req);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async static Task<TV> SendRequest<T,TV>(T request, string url, string method)
        {
            var jsonReqStr = new myJsonData();
            jsonReqStr.url = serviceUrl + url;
            jsonReqStr.body = request != null ? JsonConvert.SerializeObject(request) : String.Empty;
            jsonReqStr.methodType = method;
            jsonReqStr.headerMap = new Dictionary<string, string>();
            jsonReqStr.headerMap.Add("Authorization", "Basic d2luZG93czpFNDBiMlNCSFl0YUU5N00zNGM2MQ==");
            jsonReqStr.headerMap.Add("Content-Type", "application/json");
            string sessionToken;
            if (IsolatedStorageSettings.ApplicationSettings.TryGetValue("sessionToken", out sessionToken))
            {
                jsonReqStr.headerMap.Add("sessionToken", sessionToken);
            }
            string jsonReq = JsonConvert.SerializeObject(jsonReqStr);
            Debug.WriteLine("Dec :" + jsonReq);
            string jsonResult = CipherUtility.getEncryptedData(jsonReq);
            //string jsonResult = CipherUtility.Encrypt<AesManaged>(jsonReq, "verisun.2014", "0123456789123456");
            Debug.WriteLine("Enc :" + jsonResult);

            var endUrl = baseUrl + "/crypto/rest/decryption";

            var httpClient = new HttpClient();
            var asdasd = await httpClient.GetAsync(new Uri(endUrl, UriKind.RelativeOrAbsolute));
            var txt = await asdasd.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TV>(txt);
        }
    }
}
