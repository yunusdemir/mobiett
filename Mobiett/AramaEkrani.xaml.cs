﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Runtime.Serialization.Json;
using System.Windows.Media;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Microsoft.Phone.Controls;
using Mobiett.Models;
using Mobiett.Framework;
using System.IO.IsolatedStorage;

namespace Mobiett
{
    public partial class AramaEkrani : PhoneApplicationPage
    {

        public string url = "http://159.8.34.170:8181";
        public Uri uri = new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute);
        public AramaEkrani()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(AramaEkrani_Loaded);
        }

        void AramaEkrani_Loaded(object sender, RoutedEventArgs e)
        {

            textBox1.Focus();
            if (IsolatedStorageSettings.ApplicationSettings.Contains("aramaBusStops"))
            {
                int j = 0;
                List<BusStopNodesModel.BusStopNodes> busList = (List<BusStopNodesModel.BusStopNodes>)IsolatedStorageSettings.ApplicationSettings["aramaBusStops"];
                foreach (var item in busList)
                {
                    if (j % 2 == 0)
                    {
                        item.color = "#FFF7F7F7";
                    }
                    else
                    {
                        item.color = "#FFF";
                    }

                    j++;
                }
                Dispatcher.BeginInvoke(() =>
                {
                    listBox1.ItemsSource = busList;
                });
            }

        }
        public class Node
        {
            public string code { get; set; }
            public string description { get; set; }
            public string garage { get; set; }
            public int journeyDuration { get; set; }
            public string name { get; set; }
            public string type { get; set; }
            public List<string> directions { get; set; }
            public string id { get; set; }
            public string county { get; set; }
            public double? latitude { get; set; }
            public double? longitude { get; set; }
            public string directionDescription { get; set; }
            public bool? isMetrobusStop { get; set; }
        }

        public class RootObject
        {
            public string type { get; set; }
            public Node node { get; set; }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            //base.OnBackKeyPress(e);
            NavigationService.Navigate(this.uri);
            //e.Cancel = true;
        }

        private void textBox1_TextChanged(object sender, TextChangedEventArgs e)
        {
            var val = textBox1.Text;
            if (val.Length >= 1)
            {
                int cursorLocation = textBox1.SelectionStart;
                textBox1.Text = textBox1.Text.ToUpper();
                textBox1.SelectionStart = cursorLocation;

                HttpOperation.SendRequest<object, List<BusStopNodesModel.BusStopNodes>>(null, "/search/" + val + "?cityId=" + City.Current.id, "GET", Response_Completed1);

            }
        }

        private void Response_Completed1(List<BusStopNodesModel.BusStopNodes> arg1, Exception arg2)
        {
            int j = 0;
            var busNodes = arg1;
            if (arg1 != null)
                foreach (var item in busNodes)
                {
                    if (j % 2 == 0)
                    {
                        item.color = "#FFF7F7F7";
                    }
                    else
                    {
                        item.color = "#FFF";
                    }
                    string _name = item.node.name;
                    if (_name.Length > 60)
                    {
                        item.node.name = item.node.name.Substring(0, 60) + "...";
                    }
                    if (item.type == "BusStop")
                    {
                        item.image = "/Mobiett;component/Images/icon_busstop@2x.png";
                        item.node.directionDescription = " " + item.node.directionDescription + " YÖNÜ";
                        item.node.lineCode = "";
                        item.node.mrgn = new Thickness(10, 8, 0, 0);
                    }
                    else
                    {
                        item.image = "/Mobiett;component/Images/icon_bus@2x1.png";
                        item.node.lineCode = item.node.code;
                        item.node.directionDescription = "";
                        item.node.mrgn = new Thickness(10, 32, 0, 0);
                    }
                    j++;
                }
            Dispatcher.BeginInvoke(() => listBox1.ItemsSource = busNodes);
        }


        private void image2_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri Uri = new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(Uri);
        }

        private void textBox1_GotFocus(object sender, RoutedEventArgs e)
        {
            textBox1.Background = new SolidColorBrush(Colors.Transparent);
            textBox1.BorderBrush = new SolidColorBrush(Colors.Transparent);
            textBox1.Foreground = new SolidColorBrush(Colors.Black);
        }

        public class myJsonData { public string methodType; public string url; public Dictionary<string, string> headerMap;}

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            int selStart = textBox1.SelectionStart;
            string _text = textBox1.Text;
            textBox1.SelectionStart = selStart++;
            e.Handled = true;
            var val = textBox1.Text;
            if (val.Length >= 1)
            {
                //int cursorLocation = textBox1.SelectionStart;
                //textBox1.SelectionStart = cursorLocation++;

                HttpOperation.SendRequest<object, List<BusStopNodesModel.BusStopNodes>>(null, "/search/" + val + "?cityId=" + City.Current.id, "GET", Response_Completed1);
            }
        }



        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BusStopNodesModel.BusStopNodes m = listBox1.SelectedItem as BusStopNodesModel.BusStopNodes;
            if (m != null)
            {
                if (m.type == "BusStop")
                {
                    if (IsolatedStorageSettings.ApplicationSettings.Contains("aramaBusStops"))
                    {
                        List<BusStopNodesModel.BusStopNodes> busList = (List<BusStopNodesModel.BusStopNodes>)IsolatedStorageSettings.ApplicationSettings["aramaBusStops"];
                        bool contCont = false;
                        foreach (var item in busList)
                        {
                            contCont = item.node.code == m.node.code ? true : false;
                            if (contCont)
                                break;
                        }
                        if (!contCont)
                        {
                            busList.Add(m);
                        }
                        IsolatedStorageSettings.ApplicationSettings["aramaBusStops"] = busList;
                    }
                    else
                    {
                        IsolatedStorageSettings.ApplicationSettings["aramaBusStops"] = new List<BusStopNodesModel.BusStopNodes>();
                        var busList = (List<BusStopNodesModel.BusStopNodes>)IsolatedStorageSettings.ApplicationSettings["aramaBusStops"];
                        busList.Add(m);
                        IsolatedStorageSettings.ApplicationSettings["aramaBusStops"] = busList;
                    }
                    string _msg = m.node.name + "," + m.node.id + " , " + m.node.directionDescription + "," + m.node.code;
                    NavigationService.Navigate(new Uri("/DurakDetay.xaml?msg=" + _msg, UriKind.Relative));
                }
                else
                {
                    if (IsolatedStorageSettings.ApplicationSettings.Contains("aramaBusStops"))
                    {
                        var busList = (List<BusStopNodesModel.BusStopNodes>)IsolatedStorageSettings.ApplicationSettings["aramaBusStops"];
                        bool contCont = false;
                        foreach (var item in busList)
                        {
                            contCont = item.node.code == m.node.code ? true : false;
                            if (contCont)
                                break;
                        }
                        if (!contCont)
                        {
                            busList.Add(m);
                        }
                        IsolatedStorageSettings.ApplicationSettings["aramaBusStops"] = busList;
                    }
                    else
                    {
                        IsolatedStorageSettings.ApplicationSettings["aramaBusStops"] = new List<BusStopNodesModel.BusStopNodes>();
                        var busList = (List<BusStopNodesModel.BusStopNodes>)IsolatedStorageSettings.ApplicationSettings["aramaBusStops"];
                        busList.Add(m);
                        IsolatedStorageSettings.ApplicationSettings["aramaBusStops"] = busList;
                    }
                    string _msg = m.node.code + "," + m.node.name + " , " + m.node.directionDescription + " , " + m.node.id;
                    NavigationService.Navigate(new Uri("/OtobusDetay.xaml?msg=" + _msg, UriKind.Relative));
                }
            }
        }

        private void textBox1_GotFocus_1(object sender, RoutedEventArgs e)
        {
            textBox1.Background = new SolidColorBrush(Colors.Transparent);
            textBox1.BorderBrush = new SolidColorBrush(Colors.Transparent);
            textBox1.Foreground = new SolidColorBrush(Colors.White);
        }

        private void StackPanel_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Node nd = new Node();
        }
    }
}