﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Windows;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Notification;
using Microsoft.Phone.Tasks;
using Mobiett.Models;
using Mobiett.Resources;

namespace Mobiett
{
    public partial class MainPage : PhoneApplicationPage
    {
        private static NavigationService navigationService;
        public MainPage()
        {
            InitializeComponent();
            PushNotificationinitializer();
            HttpOperation.Dispatcher = Dispatcher;
            Loaded += (sender, args) => navigationService = NavigationService;
        }

        private void ContentPanel_Loaded(object sender, RoutedEventArgs e)
        {
            if (Constants.PushInfo.PushChannel_URI == null) return;
        }

        private void callback(InitResponse arg1, Exception arg2)
        {
            if (arg2 != null)
            {
                MessageBox.Show(arg2.Message);
                return;
            }
            IsolatedStorageSettings.ApplicationSettings["DeviceId"] = arg1.id;
            IsolatedStorageSettings.ApplicationSettings["sessionToken"] = arg1.sessionToken;
            if (arg1.version.mustBeUpdated || !arg1.version.isLatest)
            {
                var message = arg1.version.versionMessages.Single(q => q.languageCode == "tr");
                if (MessageBox.Show(message.updateAvailableMessage, LangugeResource.PopUpHeader_Caution, MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                {
                    if (arg1.version.mustBeUpdated)
                    {
                        App.Current.Terminate();
                    }
                    else
                    {
                        GetCities();
                    }
                }
                else
                {
                    WebBrowserTask webBrowserTask = new WebBrowserTask();

                    webBrowserTask.Uri = new Uri(arg1.version.url, UriKind.Absolute);

                    webBrowserTask.Show();
                }
            }
            GetCities();
        }

        private void GetCities()
        {
            HttpOperation.SendRequest<object, List<City>>(null, "/cities", "GET", callback2);
        }
        private void NavigateToAnasayfa()
        {
            if (!IsolatedStorageSettings.ApplicationSettings.Contains(Constants.ISKeys.FirstLaunch))
            {
                IsolatedStorageSettings.ApplicationSettings.Add(Constants.ISKeys.FirstLaunch,true);
                Yardimnew.FirstLaunch = true;
                NavigationService.Navigate(new Uri("/Yardimnew.xaml", UriKind.RelativeOrAbsolute));
                return;
            }
            NavigationService.Navigate(new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute));
        }

        private void callback2(List<City> arg1, Exception arg2)
        {
            if (!IsolatedStorageSettings.ApplicationSettings.Contains(Constants.ISKeys.SelectedCity))
                IsolatedStorageSettings.ApplicationSettings.Add(Constants.ISKeys.SelectedCity, arg1.First());

            City.Current = IsolatedStorageSettings.ApplicationSettings[Constants.ISKeys.SelectedCity] as City;

            NavigateToAnasayfa();
        }

        #region PushNotification

        private void PushNotificationinitializer()
        {
            try
            {

                // Try to find the push channel.
                Constants.PushInfo.pushChannel = HttpNotificationChannel.Find(Constants.PushInfo.PushChannel_Name);
            }
            catch (Exception)
            {
                // TODO Handle Exception
            }

            // If the channel was not found, then create a new connection to the push service.
            if (Constants.PushInfo.pushChannel == null)
            {
                Constants.PushInfo.pushChannel = new HttpNotificationChannel(Constants.PushInfo.PushChannel_Name);

                // Register for all the events before attempting to open the channel.
                Constants.PushInfo.pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                Constants.PushInfo.pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);

                // Register for this notification only if you need to receive the notifications while your application is running.
                Constants.PushInfo.pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(PushChannel_ShellToastNotificationReceived);

                Constants.PushInfo.pushChannel.Open();

                // Bind this new channel for toast events.
                Constants.PushInfo.pushChannel.BindToShellToast();
            }
            else
            {
                // The channel was already open, so just register for all the events.
                Constants.PushInfo.pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                Constants.PushInfo.pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);

                // Register for this notification only if you need to receive the notifications while your application is running.
                Constants.PushInfo.pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(PushChannel_ShellToastNotificationReceived);

                try
                {
                    // Display the URI for testing purposes. Normally, the URI would be passed back to your web service at this point.
                    Debug.WriteLine(String.Format("Channel Uri is {0}",
                        Constants.PushInfo.pushChannel.ChannelUri.ToString()));
                }
                catch (Exception ex)
                {
                    // TODO Handle Exception
                }
            }
            Constants.PushInfo.PushChannel_URI = Constants.PushInfo.pushChannel.ChannelUri;
            if (Constants.PushInfo.PushChannel_URI != null)
            {
                InitService(Constants.PushInfo.PushChannel_URI.ToString());
                Debug.WriteLine(Constants.PushInfo.PushChannel_URI.ToString());
            }
        }

        private void PushChannel_ShellToastNotificationReceived(object sender, NotificationEventArgs e)
        {
            //StringBuilder message = new StringBuilder();
            //string relativeUri = string.Empty;

            //message.AppendFormat("Received Toast {0}:\n", DateTime.Now.ToShortTimeString());

            //// Parse out the information that was part of the message.
            //foreach (string key in e.Collection.Keys)
            //{
            //    message.AppendFormat("{0}: {1}\n", key, e.Collection[key]);

            //    if (string.Compare(
            //        key,
            //        "wp:Param",
            //        System.Globalization.CultureInfo.InvariantCulture,
            //        System.Globalization.CompareOptions.IgnoreCase) == 0)
            //    {
            //        relativeUri = e.Collection[key];
            //    }
            //}

            //if (App.Current!=null)
            //{
            Dispatcher.BeginInvoke(() => MessageBox.Show(e.Collection.Skip(1).First().Value, e.Collection.First().Value, MessageBoxButton.OK));
            //}
            //else
            //{
            //    var shellToast=new ShellToast();
            //    //shellToast.Title = "title";
            //    shellToast.Content = message.ToString();
            //    shellToast.Show();
            //}
            // Display a dialog of all the fields in the toast.

        }

        private void PushChannel_ErrorOccurred(object sender, NotificationChannelErrorEventArgs e)
        {
        }

        private async void PushChannel_ChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {
            Constants.PushInfo.PushChannel_URI = e.ChannelUri;
            Debug.WriteLine(Constants.PushInfo.PushChannel_URI.ToString());

            InitService(e.ChannelUri.ToString());
        }

        #endregion PushNotification

        public void InitService(string pushToken)
        {
            Init init = new Init { version = new Init.Version { code = HelperFunctions.GetApplicationVersion() }, token = pushToken };
            HttpOperation.SendRequest<Init, InitResponse>(init, "/devices", "PUT", callback);
        }

        public static void Navigate(string page)
        {
            navigationService.Navigate(new Uri(page, UriKind.RelativeOrAbsolute));
        }
    }
}