﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.Text;
using System.Windows.Media.Imaging;
using Mobiett.Models;
using Mobiett.Resources;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace Mobiett
{
    public partial class AlarmList : PhoneApplicationPage
    {
        public List<Alarm> Alarms { get; set; }
        public AlarmList()
        {
            DataContext = this;
            InitializeComponent();
            this.Loaded += AlarmList_Loaded;
        }
        private List<string> days = new List<string>
        {
            LangugeResource.Monday,
            LangugeResource.Tuesday,
            LangugeResource.Wednesday,
            LangugeResource.Thursday,
            LangugeResource.Friday,
            LangugeResource.Saturday,
            LangugeResource.Sunday,
        };
        void AlarmList_Loaded(object sender, RoutedEventArgs e)
        {
            string deviceId;
            IsolatedStorageSettings.ApplicationSettings.TryGetValue("DeviceId", out deviceId);
            animStart();
            HttpOperation.SendRequest<object, List<Alarm>>(null, string.Format("/devices/{0}/advancedalarm", deviceId), "GET", callback);
        }

        private void callback(List<Alarm> alarms, Exception arg2)
        {
            AnimStop();
            foreach (var alarm in alarms)
            {
                var sBuilder = new StringBuilder();
                if (alarm.repeatedDays.Equals("XXXXX__"))
                {
                    alarm.repeatedDays = "Haftaiçi";
                    continue;
                }

                if (alarm.repeatedDays.Equals("_____XX"))
                {
                    alarm.repeatedDays = "HaftaSonu";
                    continue;
                }

                for (var i = 0; i < alarm.repeatedDays.Length; i++)
                {
                    if (alarm.repeatedDays[i] == 'X')
                    {
                        sBuilder.Append(days[i] + ", ");
                    }
                }

                alarm.repeatedDays = sBuilder.ToString();
                alarm.repeatedDays = alarm.repeatedDays.Remove(alarm.repeatedDays.Length - 2);
            }
            Alarms = alarms;
            listBox2.ItemsSource = alarms;

        }


        private void Image_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AddAlarm.xaml", UriKind.RelativeOrAbsolute));
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var alarm = btn.Tag as Alarm;
            AddAlarm.id = alarm.id;
            AddAlarm.line = new BusStopNodesModel.Node
            {
                id = alarm.busLine.id,
                name = alarm.busLine.name,
                code = alarm.busLine.code,
                directionDescription = alarm.direction,
                directions = alarm.busLine.directions.ToList()
            };
            AddAlarm.stop = new BusDirectionsModel.Stop
            {
                id = alarm.busStop.id,
                name = alarm.busStop.name,
                code = alarm.busStop.code,
                directionDescription = alarm.direction
            };
            AddAlarm.days = alarm.repeatedDays;
            AddAlarm.startTime = DateTime.ParseExact(alarm.startTime, "hh:mm:ss", new CultureInfo("tr-TR"));
            AddAlarm.endTime = DateTime.ParseExact(alarm.endTime, "hh:mm:ss", new CultureInfo("tr-TR"));
            AddAlarm.alarmTime = alarm.alarmTime;
            AddAlarm.isActive = alarm.isActive;
            NavigationService.Navigate(new Uri("/AddAlarm.xaml", UriKind.RelativeOrAbsolute));
        }

        private void UIElement_OnTap(object sender, GestureEventArgs e)
        {
            NavigationService.GoBack();
        }


        private void CheckStatusChanged(object sender, GestureEventArgs e)
        {
            var tg = sender as ToggleSwitch;
            var alarm = tg.Tag as Alarm;
            alarm.isActive = tg.IsChecked.Value;
            var deviceId = IsolatedStorageSettings.ApplicationSettings["DeviceId"];
            animStart();
            HttpOperation.SendRequest<Alarm, Alarm>(alarm, String.Format("/devices/{0}/advancedalarm/{1}", deviceId, alarm.id), "PUT", callback);
        }

        private void callback(Alarm arg1, Exception arg2)
        {
            AnimStop();
            if (arg2 != null)
                MessageBox.Show(LangugeResource.ServiceException_ExceptionOccured);
            else
                MessageBox.Show(LangugeResource.PopupMessage_TimerUpdated);
        }
        private int ca = 2;
        private void Anime_Completed(object sender, EventArgs e)
        {
            string url = "/Mobiett;component/Images/Loading/loading" + ca + ".png";
            Dispatcher.BeginInvoke(() =>
            {
                imageAnime.Source = new BitmapImage(new Uri(url, UriKind.Relative));
                if (imageAnime.Visibility == Visibility.Visible)
                {
                    ca++;
                    if (ca == 46)
                    {
                        ca = 0;
                    }
                    StoryBoardA.Begin();
                }
            });
        }

        private void animStart()
        {
            Dispatcher.BeginInvoke(() =>
            {
                loadAnim.Visibility = Visibility.Visible;
                StoryBoardA.Begin();
            });
        }

        private void AnimStop()
        {

            Dispatcher.BeginInvoke(() =>
            {
                StoryBoardA.Stop();
                loadAnim.Visibility = Visibility.Collapsed;
            });
        }
    }
}