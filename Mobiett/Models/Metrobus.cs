﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobiett.Models
{


    public class MetrobusModel
    {
        public string code { get; set; }
        public string county { get; set; }
        public float latitude { get; set; }
        public float longitude { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string directionDescription { get; set; }
        public bool isMetrobusStop { get; set; }
        public string id { get; set; }
    }

}
