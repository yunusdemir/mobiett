﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobiett.Models
{
    public class SideMenu
    {
        public string imageName { get; set; }
        public string url { get; set; }
        public string type { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public Advertisementmessage[] advertisementMessages { get; set; }
        public string imageUrl { get; set; }
        public string id { get; set; }
    }
}

