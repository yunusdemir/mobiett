﻿namespace Mobiett.Models
{

    public class Alarm
    {
        public int alarmTime { get; set; }
        public string languageCode { get; set; }
        public Busline busLine { get; set; }
        public string direction { get; set; }
        public Busstop busStop { get; set; }
        public Device device { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string repeatedDays { get; set; }
        public string id { get; set; }
        public bool isActive { get; set; }
    }

    public class Busline
    {
        public string code { get; set; }
        public string description { get; set; }
        public string garage { get; set; }
        public int journeyDuration { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string[] directions { get; set; }
        public string id { get; set; }
    }

    public class Busstop
    {
        public string code { get; set; }
        public string county { get; set; }
        public float latitude { get; set; }
        public float longitude { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string directionDescription { get; set; }
        public bool isMetrobusStop { get; set; }
        public string id { get; set; }
    }

    public class Device
    {
        public string token { get; set; }
        public string type { get; set; }
        public string lastLogin { get; set; }
        public string id { get; set; }
    }

    public class Verssion
    {
        public string deviceType { get; set; }
        public string code { get; set; }
        public bool isLatest { get; set; }
        public bool mustBeUpdated { get; set; }
        public string url { get; set; }
        public Versionmessage[] versionMessages { get; set; }
        public string id { get; set; }
    }

    public class Versionmessage
    {
        public string languageCode { get; set; }
        public string updateAvailableMessage { get; set; }
        public string forceUpdateMessage { get; set; }
        public string id { get; set; }
    }

}
