﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Mobiett.Models
{
    public class BuslineTimetableModel
    {
        public class BusLine
        {
            public string code { get; set; }
            public string description { get; set; }
            public string garage { get; set; }
            public int journeyDuration { get; set; }
            public string name { get; set; }
            public string type { get; set; }
            public List<string> directions { get; set; }
            public string id { get; set; }
          
        }

        public class LineSchedule
        {
            public string dayType { get; set; }
            public string direction { get; set; }
            public string time { get; set; }
            public string timenew { get; set; }
            public bool isPublicBus { get; set; }
            public string markCode { get; set; }
            public string id { get; set; }
        }

        public class Description
        {
            public string direction { get; set; }
            public string description { get; set; }
            public string id { get; set; }
        }

        public class BuslineTimetable
        {
            public Description direction { get; set; } 
            public BusLine busLine { get; set; }
            public List<LineSchedule> lineSchedules { get; set; }
            public List<Description> descriptions { get; set; }
        }
    }
}
