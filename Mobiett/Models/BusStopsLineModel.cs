﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Mobiett.Models
{
    public class BusStopsLineModel
    {
        public class Line
        {
            public string code { get; set; }
            public int journeyDuration { get; set; }
            public string name { get; set; }
            public string type { get; set; }
            public List<string> directions { get; set; }
            public string id { get; set; }
            public string typeMode { get; set; }
            public string kalanSure { get; set; }
            public string prop0 { get; set; }
            public string prop1 { get; set; }
            public string prop2 { get; set; }
            public string prop3 { get; set; }
        }

        public class BusStopsLine
        {
            public string dataType { get; set; }
            public Line line { get; set; }
            public string doorNo { get; set; }
            public int timeToStop { get; set; }
            public int stopCount { get; set; }
            public List<string> properties { get; set; }
            public double distanceToStop { get; set; }
            public string color { get; set; }
            
        }
    }
}
