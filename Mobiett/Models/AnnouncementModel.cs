﻿namespace Mobiett.Models
{
    public class AnnouncementModel
    {
        public string title { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string id { get; set; }
    }

}
