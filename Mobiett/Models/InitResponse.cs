﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobiett.Models
{

    public class VersionMessage
    {
        public string languageCode { get; set; }
        public string updateAvailableMessage { get; set; }
        public string forceUpdateMessage { get; set; }
        public string id { get; set; }
    }

    public class Version
    {
        public string deviceType { get; set; }
        public string code { get; set; }
        public bool isLatest { get; set; }
        public bool mustBeUpdated { get; set; }
        public string url { get; set; }
        public List<VersionMessage> versionMessages { get; set; }
        public string id { get; set; }
    }

    public class InitResponse
    {
        public string token { get; set; }
        public string type { get; set; }
        public string lastLogin { get; set; }
        public Version version { get; set; }
        public string id { get; set; }
        public string sessionToken { get; set; }
    }
}
