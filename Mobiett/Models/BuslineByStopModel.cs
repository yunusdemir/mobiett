﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Mobiett.Models
{
    public class BuslineByStopModel
    {
        public class BuslineByStop
        {
            public string code { get; set; }
            public string description { get; set; }
            public string garage { get; set; }
            public int journeyDuration { get; set; }
            public string name { get; set; }
            public string type { get; set; }
            public List<string> directions { get; set; }
            public string id { get; set; }
        }
    }
}
