﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Mobiett.Models
{
    public class BusStop
    {
        public string code { get; set; }
        public string county { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string directionDescription { get; set; }
        public bool isMetrobusStop { get; set; }
        public string id { get; set; }
    }

    public class BusStops
    {
        public BusStop busStop { get; set; }
        public double distance { get; set; }
        public string distanceString { get; set; }
        public string code { get; set; }
        public string county { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string directionDescription { get; set; }
        public bool isMetrobusStop { get; set; }
        public string id { get; set; }
    }
}
