﻿namespace Mobiett.Models
{

    public class City
    {
        public string code { get; set; }
        public string name { get; set; }
        public Staticfile[] staticFiles { get; set; }
        public string id { get; set; }
        public static City Current { get; set; }
    }

    public class Staticfile
    {
        public string updateDate { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public string screenName { get; set; }
        public string id { get; set; }
    }


}
