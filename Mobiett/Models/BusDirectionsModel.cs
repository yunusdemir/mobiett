﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Mobiett.Models
{
    public class BusDirectionsModel
    {
        public class Stop
        {
            public string code { get; set; }
            public string county { get; set; }
            public double latitude { get; set; }
            public double longitude { get; set; }
            public string name { get; set; }
            public string type { get; set; }
            public string directionDescription { get; set; }
            public bool isMetrobusStop { get; set; }
            public string id { get; set; }
            public string youAreHere { get; set; }
            public string youAreHereImage { get; set; }
        }
        public class BusLocation
        {
            public double latitude { get; set; }
            public double longitude { get; set; }
        }

        public class Buses
        {
            public BusLocation busLocation { get; set; }
            public string type { get; set; }
            public string directionDescription { get; set; }
            public string order { get; set; }
            public string doorNo { get; set; }
            public int elapsedTime { get; set; }
        }
        public class BusDirections
        {
            public Stop stop { get; set; }
            public string busImage { get; set; }
            public List<Buses> buses { get; set; }
            public List<object> properties { get; set; }
        }
    }
}
