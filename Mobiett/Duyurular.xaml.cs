﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using Mobiett.Models;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace Mobiett
{
    public partial class Duyurular : PhoneApplicationPage
    {

        public Duyurular()
        {
            InitializeComponent();
            this.Loaded += Duyurular_Loaded;
        }

        void Duyurular_Loaded(object sender, RoutedEventArgs e)
        {
            animStart();
            HttpOperation.SendRequest<object, List<AnnouncementModel>>(null, "/announcements/active=true", "GET", callback);
        }

        private void callback(List<AnnouncementModel> arg1, Exception arg2)
        {
            AnimStop();
            announcementList = arg1;
            duyurulist.ItemsSource = arg1;
        }

        public List<AnnouncementModel> announcementList { get; set; }

        private void back_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.GoBack();
        }

        private int ca = 2;
        private void Anime_Completed(object sender, EventArgs e)
        {
            string url = "/Mobiett;component/Images/Loading/loading" + ca + ".png";
            Dispatcher.BeginInvoke(() =>
            {
                imageAnime.Source = new BitmapImage(new Uri(url, UriKind.Relative));
                if (imageAnime.Visibility == Visibility.Visible)
                {
                    ca++;
                    if (ca == 46)
                    {
                        ca = 0;
                    }
                    StoryBoardA.Begin();
                }
            });
        }

        private void animStart()
        {
            Dispatcher.BeginInvoke(() =>
            {
                loadAnim.Visibility = Visibility.Visible;
                StoryBoardA.Begin();
            });
        }

        private void AnimStop()
        {

            Dispatcher.BeginInvoke(() =>
            {
                StoryBoardA.Stop();
                loadAnim.Visibility = Visibility.Collapsed;
            });
        }

        private void UIElement_OnTap(object sender, GestureEventArgs e)
        {
            var sp = sender as StackPanel;
            WebBrowserTask webBrowserTask = new WebBrowserTask();

            webBrowserTask.Uri = new Uri(sp.Tag.ToString(), UriKind.Absolute);

            webBrowserTask.Show();
        }
    }
}