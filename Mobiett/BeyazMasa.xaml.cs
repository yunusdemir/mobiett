﻿using System;
using System.Windows;
using System.IO;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Controls;
using Mobiett.Models;
using Microsoft.Phone.Tasks;



namespace Mobiett
{
    public partial class BeyazMasa : PhoneApplicationPage
    {
        private string imgSource = "";
        String[] Kartlar = { "Öğretmen Kartı","Öğrenci Kartı",
                              "Sosyal Kart","Mavi Kart",
                              "Harcırah Kartı","Ticari Kart",
                              "Engelli Kartı","Refakatli Kartı","EHS Polis Kartı", "Malül Kartı", "PTT Kartı", "Denetim Kartı", "TÜİK Kartı", "Sarı Basın Kartı", "Milli Sporcu Kartı", "Gazi Kartı", "Gazi Eşi Kartı", "Şehit Anne ve Babası Kartı", "Şehit Dul ve Yetimleri Kartı", "Ordu Vazife Malülü Kartı", "Anonim Kart", "Sınırlı Kullanımlı Bilet", "EHS Zabıta Kartı", "EHS Merkez-İl Jan. Kom.", "EHS Sahil Güvenlik Kartı"};

        String[] Kategoriler = { "Seyahat Kartları","Sefer Gecikmesi/İptali",
                              "Sefer Arttırılma Talebi","Personel",
                              "Metrobüs","İETT Otobüsleri","ÖHO Otobüsleri", "Otobüs AŞ Otobüsleri","Durak","Hat ve Güzergah", "Web Sitesi" , "Ücret Tarife", "Raylı Ulaşım Sistemleri", "Kayıp Eşya", "Basın Yayın Reklam", "İnsan Kaynakları", "Teşekkür", "Elektronikkart/Akbil Dolum" };

        String[] Konular = { "Metrobüs","Seyahat Kart Uygulamaları",
                              "İETT Otobüsleri","Genel",
                              "Web Sitesi","Personel", "Yeni Hat Talebin Karşılanması", "Hattın Uzatılması", "Başvuruma cevap verilmesi"};

        String[] Hatlar = {"Konu1","Konu2",
                              "Konu3","Konu4",
                              "Konu5","Konu6" };

        private MemoryStream photoStream;
        private string fileName;
        PhotoChooserTask photoChooserTask;



        public BeyazMasa()
        {
            InitializeComponent();
            this.kartList.ItemsSource = Kartlar;
            this.hatList.ItemsSource = Hatlar;
            this.kategoriList.ItemsSource = Kategoriler;
            this.konuList.ItemsSource = Konular;

            this.photoChooserTask = new PhotoChooserTask();
            this.photoChooserTask.Completed += new EventHandler<PhotoResult>(photoChooserTask_Completed);

        }


        private void button1_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

            photoChooserTask.Show();


        }

        private void photoChooserTask_Completed(object sender, PhotoResult e)
        {
            
            if (e.TaskResult == TaskResult.OK)
            {
                BitmapImage image = new BitmapImage();
                image.SetSource(e.ChosenPhoto);
                this.img.Source = image;
                this.imgSource = e.OriginalFileName;
                var flArray = e.OriginalFileName.Split('\\');
                string fileName = flArray[flArray.Length - 1];
                //WriteableBitmap selectedPhoto = PictureDecoder.DecodeJpeg(e.ChosenPhoto);
                Dispatcher.BeginInvoke(() => textblckimg.Text = fileName);
                buttonsil.Visibility = Visibility.Visible;
                button1.Visibility = Visibility.Collapsed;
                gridTxtImage.Visibility = Visibility.Visible;
            }
            
        }



        private void photoCameraCapture_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                BitmapImage image = new BitmapImage();
                image.SetSource(e.ChosenPhoto);
                this.img.Source = image;
            }
        }

        private void buttonsil_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.imgSource = "";
            Dispatcher.BeginInvoke(() => textblckimg.Text = "");
            buttonsil.Visibility = Visibility.Collapsed;
            button1.Visibility = Visibility.Visible;
            gridTxtImage.Visibility = Visibility.Collapsed;
        }

        
    }
}
