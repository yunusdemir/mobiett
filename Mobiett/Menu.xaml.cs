﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Runtime.Serialization.Json;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using System.Text;
using System.Device;
using System.Windows.Threading;
using System.Globalization;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Shell;
using System.Diagnostics;
using System.Device.Location;
using Windows.Devices.Geolocation;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Controls.Maps.ConfigService;
using Microsoft.Phone.Reactive;
using Microsoft.Phone.Scheduler;
using System.IO.IsolatedStorage; 


namespace Mobiett
{
    public partial class Menu : PhoneApplicationPage
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void image1_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri uri = new Uri("/AnaEkran.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }

        private void TextBlock_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri uri = new Uri("/iettHakkindaEkrani.xaml", UriKind.RelativeOrAbsolute);
            NavigationService.Navigate(uri);
        }

        private void image1_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }
    }
}