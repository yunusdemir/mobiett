﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using Microsoft.Phone.Controls;
using System.Device.Location;
using Mobiett.Models;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using System.IO;
using Mobiett.Framework;
using System.Text;
using System.Runtime.Serialization.Json;
using Microsoft.Phone.Controls.Maps;
using System.Xml;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Maps.Controls;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace Mobiett
{
    public partial class Metrobus : PhoneApplicationPage
    {
        PushpinViewModel2 _viewModel;
        public ContextMenu contextMenum { get; set; }


        public Metrobus()
        {
            InitializeComponent();
            HttpOperation.Dispatcher = Dispatcher;
        }

        public void LineMap_OnLoaded(object sender, RoutedEventArgs e)
        {
            //animStart();
            HttpOperation.SendRequest<object, List<MetrobusModel>>(null, "/busstops/type/metrobus" + "?cityId=" + City.Current.id, "GET", callback);
        }

        private void callback(List<MetrobusModel> arg1, Exception arg2)
        {
            //AnimStop();
            metrobusList = arg1;
            _viewModel = new PushpinViewModel2(arg1);
            mapPins.ItemsSource = _viewModel.Pushpins;
            lineMap.SetView(new GeoCoordinate(metrobusList.Average(q=>q.latitude), metrobusList.Average(q=>q.longitude)), 10);


            //double tLat = 0;
            //double tLong = 0;
            //foreach (var metrobuse in arg1)
            //{
            //    var pushpin = new Pushpin();
            //    pushpin.PositionOrigin = PositionOrigin.BottomCenter;
            //    pushpin.Location = new GeoCoordinate(metrobuse.latitude, metrobuse.longitude);
            //    tLat += metrobuse.latitude;
            //    tLong += metrobuse.longitude;
            //    pushpin.Style = this.Resources["PushpinStyle"] as Style;
            //    pushpin.Tag = metrobuse.name;
            //    pushpin.DataContext = metrobuse.name;
            //    pushpin.Tap += (sender, args) =>
            //    {
            //        var pp = sender as Pushpin;
            //        var m = metrobusList.FirstOrDefault(q => q.name == pp.Tag.ToString());
            //        if (MessageBox.Show(pp.Tag.ToString() + " Metrobüs Durağı.\n Durak detaylarına git.","",MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            //        {
            //            var msg = string.Join(",", m.name, m.id, m.directionDescription);
            //            NavigationService.Navigate(new Uri("/DurakDetay.xaml?msg=" + msg, UriKind.Relative));
            //        }
            //    };
            //    lineMap.Children.Add(pushpin);
            //}
            //lineMap.SetView(new GeoCoordinate(tLat / arg1.Count, tLong / arg1.Count), 10);
        }

        public List<MetrobusModel> metrobusList { get; set; }

        private int ca = 2;
        private void Anime_Completed(object sender, EventArgs e)
        {
            string url = "/Mobiett;component/Images/Loading/loading" + ca + ".png";
            Dispatcher.BeginInvoke(() =>
            {
                imageAnime.Source = new BitmapImage(new Uri(url, UriKind.Relative));
                if (imageAnime.Visibility == Visibility.Visible)
                {
                    ca++;
                    if (ca == 46)
                    {
                        ca = 0;
                    }
                    StoryBoardA.Begin();
                }
            });
        }

        private void animStart()
        {
            Dispatcher.BeginInvoke(() =>
            {
                loadAnim.Visibility = Visibility.Visible;
                StoryBoardA.Begin();
            });
        }

        private void AnimStop()
        {

            Dispatcher.BeginInvoke(() =>
            {
                StoryBoardA.Stop();
                loadAnim.Visibility = Visibility.Collapsed;
            });
        }

        private void Pushpin_Tap(object sender, GestureEventArgs e)
        {
            //var _uri = new Uri("/Mobiett;component/Images/bubble@2x.png", UriKind.Relative);
            //var imb = new ImageBrush();
            //imb.ImageSource = new BitmapImage(_uri);
            _viewModel = _viewModel ?? new PushpinViewModel2(metrobusList);
            var _ppmodel = sender as Pushpin;
            ContextMenu contextMenu = ContextMenuService.GetContextMenu(_ppmodel);
            this.contextMenum = contextMenu;
            contextMenu.DataContext = _viewModel.Pushpins.Where
                (c => (c.Location
                    == _ppmodel.Location)).FirstOrDefault();
            if (contextMenu.Parent == null)
            {
                contextMenu.IsOpen = true;
            }
        }



        private void MenuItem_Tap(object sender, GestureEventArgs e)
        {
        }

        private void ContextMenu_Tap(object sender, GestureEventArgs e)
        {
            MessageBox.Show("csadak");
        }

        private void GestureListener_Tap(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            Border btn = sender as Border;
            ContextMenu contextMenu = ContextMenuService.GetContextMenu(btn);
            Pushpin pin = sender as Pushpin;
            var myTem = _viewModel.Pushpins.FirstOrDefault(c => (c.Id.Equals(btn.Tag)));
            //contextMenu.DataContext = _viewModel.Pushpins.Where(c => (c.Id.Equals(border.Tag))).FirstOrDefault();
            string _msg = myTem.Name + "," + myTem.Id + " , " + myTem.Yon;
            if (this.contextMenum != null)
            {
                this.contextMenum.IsOpen = false;
            }
            NavigationService.Navigate(new Uri("/DurakDetay.xaml?msg=" + _msg, UriKind.Relative));
        }
    }

    public class PushpinViewModel2
    {
        private ObservableCollection<PushpinModel> _pushpins = new ObservableCollection<PushpinModel>();

        public ObservableCollection<PushpinModel> Pushpins
        {
            get
            {
                return _pushpins;
            }
        }

        public PushpinViewModel2(List<BusStops> busStoplar)
        {
            InitializePushpins(busStoplar);
        }

        public PushpinViewModel2(List<MetrobusModel> busStoplar)
        {
            InitializePushpins(busStoplar);
        }

        private void InitializePushpins(List<MetrobusModel> busStoplar)
        {
            foreach (var item in busStoplar)
            {
                _pushpins.Add(new PushpinModel
                {
                    Description = "" + item.directionDescription,
                    Name = "" + item.name,
                    Location = new GeoCoordinate(item.latitude, item.longitude),
                    DatetimeAdded = DateTime.Now,
                    Id = item.id,
                    Yon = item.directionDescription + " YÖNÜ",
                    Icon = new Uri("/Mobiett;component/Images/icn_metrobusdurak.png", UriKind.Relative),
                    InsideIcon = "/Mobiett;component/Images/icon_busstop@2x.png",
                    BgCon = new Uri("/Mobiett;component/Images/bubble@2x.png", UriKind.Relative)
                });
            }
        }

        private void InitializePushpins(List<BusStops> busStoplar)
        {
            foreach (var item in busStoplar)
            {
                _pushpins.Add(new PushpinModel
                {
                    Description = "" + item.distanceString,
                    Name = "" + item.name,
                    Location = new GeoCoordinate(item.latitude, item.longitude),
                    DatetimeAdded = DateTime.Now,
                    Id = item.id,
                    Yon = item.directionDescription + " YÖNÜ",
                    Icon = new Uri("/Mobiett;component/Images/icn_durak_pin2@2x.png", UriKind.Relative),
                    InsideIcon = "/Mobiett;component/Images/icon_busstop@2x.png",
                    BgCon = new Uri("/Mobiett;component/Images/bubble@2x.png", UriKind.Relative)
                });
            }
        }
    }
}